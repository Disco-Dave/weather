module Weather.Effect exposing
    ( Dependencies
    , Effect(..)
    , map
    , toCmd
    )

import Weather.Effect.Api as Api
import Weather.Effect.Time as Time
import Weather.Url.BasePath exposing (BasePath)


type alias Dependencies =
    { apiPath : BasePath
    }


type Effect msg
    = None
    | Batch (List (Effect msg))
    | Api (Api.Effect msg)
    | Time (Time.Effect msg)


map : (oldMsg -> newMsg) -> Effect oldMsg -> Effect newMsg
map f effect =
    case effect of
        None ->
            None

        Batch effects ->
            Batch (List.map (map f) effects)

        Api apiEffect ->
            Api (Api.map f apiEffect)

        Time timeEffect ->
            Time (Time.map f timeEffect)


toCmd : Dependencies -> Effect msg -> Cmd msg
toCmd deps effect =
    case effect of
        None ->
            Cmd.none

        Batch effects ->
            List.map (toCmd deps) effects
                |> Cmd.batch

        Api apiEffect ->
            Api.toCmd apiEffect
                { apiPath = deps.apiPath
                }

        Time timeEffect ->
            Time.toCmd timeEffect
