module Weather.Utils.Time exposing (viewHumanTime)

import Time


viewHumanTime : Time.Zone -> Time.Posix -> String
viewHumanTime here posix =
    let
        year =
            Time.toYear here posix
                |> String.fromInt

        month =
            case Time.toMonth here posix of
                Time.Jan ->
                    "1"

                Time.Feb ->
                    "2"

                Time.Mar ->
                    "3"

                Time.Apr ->
                    "4"

                Time.May ->
                    "5"

                Time.Jun ->
                    "6"

                Time.Jul ->
                    "7"

                Time.Aug ->
                    "8"

                Time.Sep ->
                    "9"

                Time.Oct ->
                    "10"

                Time.Nov ->
                    "11"

                Time.Dec ->
                    "12"

        day =
            Time.toDay here posix
                |> String.fromInt

        hour =
            Time.toHour here posix
                |> String.fromInt

        minute =
            Time.toMinute here posix
                |> String.fromInt
    in
    month ++ "/" ++ day ++ "/" ++ year ++ " " ++ hour ++ ":" ++ minute
