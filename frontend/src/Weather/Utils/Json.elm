module Weather.Utils.Json exposing
    ( maybe
    , nullable
    )

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode


nullable : (a -> Encode.Value) -> Maybe a -> Encode.Value
nullable encode maybeValue =
    case maybeValue of
        Nothing ->
            Encode.null

        Just value ->
            encode value


maybe : String -> Decode.Decoder a -> Decode.Decoder (Maybe a -> b) -> Decode.Decoder b
maybe property decode =
    Pipeline.optional property (Decode.map Just decode) Nothing
