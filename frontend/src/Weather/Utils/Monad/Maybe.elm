module Weather.Utils.Monad.Maybe exposing (isJust, isNothing)


isJust : Maybe a -> Bool
isJust maybe =
    case maybe of
        Nothing ->
            False

        Just _ ->
            True


isNothing : Maybe a -> Bool
isNothing =
    isJust >> not
