module Weather.Utils.Http exposing
    ( ErrorBody(..)
    , errorString
    , errorString_
    , expectJsonBodies
    , expectMetadataAndString
    )

import Http
import Json.Decode as Decode exposing (Decoder)


type ErrorBody a
    = BadUrl String
    | BadBody Http.Metadata String
    | BadStatus Http.Metadata a
    | Timeout
    | NetworkError


errorString : (a -> String) -> ErrorBody a -> String
errorString aToString error =
    case error of
        BadUrl url ->
            "Bad url: " ++ url

        BadBody meta body ->
            "Bad body (status: " ++ String.fromInt meta.statusCode ++ "): " ++ body

        BadStatus meta errBody ->
            "Bad status (" ++ String.fromInt meta.statusCode ++ "): " ++ aToString errBody

        Timeout ->
            "Timeout"

        NetworkError ->
            "Network error"


errorString_ : ErrorBody a -> String
errorString_ error =
    case error of
        BadUrl url ->
            "Bad url: " ++ url

        BadBody meta body ->
            "Bad body (status " ++ String.fromInt meta.statusCode ++ "): " ++ body

        BadStatus meta _ ->
            "Bad status (" ++ String.fromInt meta.statusCode ++ ")"

        Timeout ->
            "Timeout"

        NetworkError ->
            "Network error"


expectMetadataAndString :
    (Http.Metadata -> String -> Result String bad)
    -> (Http.Metadata -> String -> Result String good)
    -> (Result (ErrorBody bad) good -> msg)
    -> Http.Expect msg
expectMetadataAndString parseBad parseGood toMsg =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (BadUrl url)

                Http.Timeout_ ->
                    Err Timeout

                Http.NetworkError_ ->
                    Err NetworkError

                Http.BadStatus_ metadata body ->
                    parseBad metadata body
                        |> Result.mapError (BadBody metadata)
                        |> Result.andThen (Err << BadStatus metadata)

                Http.GoodStatus_ metadata body ->
                    parseGood metadata body
                        |> Result.mapError (BadBody metadata)


expectJsonBodies :
    (Result (ErrorBody bad) good -> msg)
    -> (Http.Metadata -> Decoder bad)
    -> (Http.Metadata -> Decoder good)
    -> Http.Expect msg
expectJsonBodies toMsg badDecoder goodDecoder =
    let
        parseString decoder metadata string =
            Decode.decodeString (decoder metadata) string
                |> Result.mapError Decode.errorToString
    in
    expectMetadataAndString (parseString badDecoder) (parseString goodDecoder) toMsg
