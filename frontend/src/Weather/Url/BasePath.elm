module Weather.Url.BasePath exposing
    ( BasePath
    , absolute
    , fromString
    )

import Url.Builder as UrlBuilder exposing (QueryParameter)


type BasePath
    = BasePath (List String)


fromString : String -> BasePath
fromString string =
    String.split "/" string
        |> List.filter (not << String.isEmpty)
        |> BasePath


absolute : BasePath -> List String -> List QueryParameter -> String
absolute (BasePath basePaths) paths queryParams =
    UrlBuilder.absolute (basePaths ++ paths) queryParams
