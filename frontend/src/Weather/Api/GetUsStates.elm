module Weather.Api.GetUsStates exposing
    ( OnResponse
    , Response
    , get
    )

import Http
import Json.Decode as Decode
import Weather.Locations.UsState as UsState exposing (UsState)
import Weather.Url.BasePath as BasePath exposing (BasePath)


type alias Response =
    List UsState


decodeResponse : Decode.Decoder Response
decodeResponse =
    Decode.list UsState.decode


type alias OnResponse msg =
    Result Http.Error Response -> msg


get :
    BasePath
    -> OnResponse msg
    -> Cmd msg
get apiPath onResponse =
    let
        url =
            BasePath.absolute apiPath [ "locations", "states" ] []
    in
    Http.get
        { url = url
        , expect = Http.expectJson onResponse decodeResponse
        }
