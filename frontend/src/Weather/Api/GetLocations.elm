module Weather.Api.GetLocations exposing
    ( BadResponse
    , OnResponse
    , Request
    , Response
    , get
    )

import Http
import Json.Decode as Decode
import Url.Builder as UrlBuilder exposing (QueryParameter)
import Weather.Locations.UsCity as UsCity exposing (UsCity)
import Weather.Locations.UsCity.Name as UsCityName exposing (UsCityName)
import Weather.Locations.UsState.Abbreviation as UsStateAbbreviation exposing (UsStateAbbreviation)
import Weather.Url.BasePath as BasePath exposing (BasePath)
import Weather.Utils.Http exposing (ErrorBody, expectJsonBodies)
import Weather.Utils.Json as Json


type alias Request =
    { name : Maybe UsCityName
    , state : Maybe UsStateAbbreviation
    }


encodeRequest : Request -> List QueryParameter
encodeRequest request =
    List.filterMap identity
        [ Maybe.map (UrlBuilder.string "city" << UsCityName.toString) request.name
        , Maybe.map (UrlBuilder.string "state" << UsStateAbbreviation.toString) request.state
        ]


type alias Response =
    List UsCity


decodeResponse : Decode.Decoder Response
decodeResponse =
    Decode.list UsCity.decode


type alias BadResponse =
    { name : Maybe String
    , state : Maybe String
    }


decodeBadResponse : Decode.Decoder BadResponse
decodeBadResponse =
    Decode.succeed BadResponse
        |> Json.maybe "name" Decode.string
        |> Json.maybe "state" Decode.string


type alias OnResponse msg =
    Result (ErrorBody BadResponse) Response -> msg


get :
    BasePath
    -> OnResponse msg
    -> Request
    -> Cmd msg
get apiPath onResponse request =
    let
        url =
            BasePath.absolute apiPath [ "locations" ] (encodeRequest request)
    in
    Http.get
        { url = url
        , expect = expectJsonBodies onResponse (\_ -> decodeBadResponse) (\_ -> decodeResponse)
        }
