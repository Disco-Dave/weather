module Weather.Api.GetForecast exposing
    ( OnResponse
    , Request
    , Response
    , get
    )

import Http
import Weather.Locations.Forecast as Forecast exposing (Forecast)
import Weather.Locations.UsCity.Id as UsCityId exposing (UsCityId)
import Weather.Url.BasePath as BasePath exposing (BasePath)


type alias Request =
    UsCityId


type alias Response =
    Forecast


type alias OnResponse msg =
    Result Http.Error Response -> msg


get :
    BasePath
    -> OnResponse msg
    -> Request
    -> Cmd msg
get apiPath onResponse cityId =
    let
        cityIdPath =
            UsCityId.toInt cityId
                |> String.fromInt

        url =
            BasePath.absolute apiPath [ "locations", cityIdPath, "weather" ] []
    in
    Http.get
        { url = url
        , expect = Http.expectJson onResponse Forecast.decode
        }
