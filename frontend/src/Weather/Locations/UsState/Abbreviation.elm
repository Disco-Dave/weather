module Weather.Locations.UsState.Abbreviation exposing
    ( Error(..)
    , UsStateAbbreviation
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Json.Decode as Decode
import Json.Encode as Encode


type UsStateAbbreviation
    = UsStateAbbreviation String


type Error
    = Empty
    | NotExactlyTwoLetters
    | ContainsNonLetters


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "US state abbreviation is empty."

        NotExactlyTwoLetters ->
            "US state abbreviation is not exactly two letters in length."

        ContainsNonLetters ->
            "US state abbreviation contains non-letters."


fromString : String -> Result (List Error) UsStateAbbreviation
fromString rawString =
    let
        normalizedString =
            String.trim rawString
                |> String.toUpper

        length =
            String.length normalizedString

        emptyCheck =
            if String.isEmpty normalizedString then
                Just Empty

            else
                Nothing

        notExactlyTwoLettersCheck =
            if length > 0 && length /= 2 then
                Just NotExactlyTwoLetters

            else
                Nothing

        containsNonLettersCheck =
            if String.all Char.isAlpha normalizedString then
                Nothing

            else
                Just ContainsNonLetters

        allChecks =
            List.filterMap identity
                [ emptyCheck
                , notExactlyTwoLettersCheck
                , containsNonLettersCheck
                ]
    in
    if List.isEmpty allChecks then
        Ok (UsStateAbbreviation normalizedString)

    else
        Err allChecks


toString : UsStateAbbreviation -> String
toString (UsStateAbbreviation value) =
    value


encode : UsStateAbbreviation -> Encode.Value
encode =
    toString >> Encode.string


decode : Decode.Decoder UsStateAbbreviation
decode =
    let
        parseString rawString =
            case fromString rawString of
                Ok value ->
                    Decode.succeed value

                Err errors ->
                    errors
                        |> List.map errorString
                        |> String.join ", "
                        |> Decode.fail
    in
    Decode.andThen parseString Decode.string
