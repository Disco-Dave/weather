module Weather.Locations.Forecast exposing
    ( Forecast
    , decode
    , encode
    )

import Iso8601
import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode
import Time
import Weather.Locations.Forecast.Temperature exposing (Fahrenheit(..), fahrenheitDecode, fahrenheitEncode)


type alias Forecast =
    { city : String
    , state : String
    , temperature : Fahrenheit
    , condition : String
    , lastUpdatedAt : Time.Posix
    }


encode : Forecast -> Encode.Value
encode weather =
    Encode.object
        [ ( "city", Encode.string weather.city )
        , ( "name", Encode.string weather.state )
        , ( "temperature", fahrenheitEncode weather.temperature )
        , ( "condition", Encode.string weather.condition )
        , ( "lastUpdatedAt", Iso8601.encode weather.lastUpdatedAt )
        ]


decode : Decode.Decoder Forecast
decode =
    Decode.succeed Forecast
        |> Pipeline.required "city" Decode.string
        |> Pipeline.required "state" Decode.string
        |> Pipeline.required "temperature" fahrenheitDecode
        |> Pipeline.required "condition" Decode.string
        |> Pipeline.required "lastUpdatedAt" Iso8601.decoder
