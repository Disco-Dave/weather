module Weather.Locations.UsState exposing
    ( UsState
    , decode
    , encode
    )

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode


type alias UsState =
    { abbreviation : String
    , name : String
    }


encode : UsState -> Encode.Value
encode usState =
    Encode.object
        [ ( "abbreviation", Encode.string usState.abbreviation )
        , ( "name", Encode.string usState.name )
        ]


decode : Decode.Decoder UsState
decode =
    Decode.succeed UsState
        |> Pipeline.required "abbreviation" Decode.string
        |> Pipeline.required "name" Decode.string
