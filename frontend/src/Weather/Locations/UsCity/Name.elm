module Weather.Locations.UsCity.Name exposing
    ( Error(..)
    , UsCityName
    , decode
    , encode
    , errorString
    , fromString
    , toString
    )

import Json.Decode as Decode
import Json.Encode as Encode


type UsCityName
    = UsCityName String


type Error
    = Empty
    | LongerThan50Characters


errorString : Error -> String
errorString error =
    case error of
        Empty ->
            "City name may not be empty."

        LongerThan50Characters ->
            "City name may not be longer than 50 characters."


fromString : String -> Result Error UsCityName
fromString string =
    let
        normalized =
            String.trim string
    in
    if String.isEmpty normalized then
        Err Empty

    else if String.length normalized > 50 then
        Err LongerThan50Characters

    else
        Ok (UsCityName normalized)


toString : UsCityName -> String
toString (UsCityName city) =
    city


encode : UsCityName -> Encode.Value
encode =
    toString >> Encode.string


decode : Decode.Decoder UsCityName
decode =
    let
        parseString rawString =
            case fromString rawString of
                Ok value ->
                    Decode.succeed value

                Err error ->
                    Decode.fail (errorString error)
    in
    Decode.andThen parseString Decode.string
