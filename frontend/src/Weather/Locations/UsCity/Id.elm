module Weather.Locations.UsCity.Id exposing
    ( UsCityId(..)
    , decode
    , encode
    , fromInt
    , toInt
    )

import Json.Decode as Decode
import Json.Encode as Encode


type UsCityId
    = UsCityId Int


toInt : UsCityId -> Int
toInt (UsCityId id) =
    id


fromInt : Int -> UsCityId
fromInt =
    UsCityId


encode : UsCityId -> Encode.Value
encode =
    toInt >> Encode.int


decode : Decode.Decoder UsCityId
decode =
    Decode.map fromInt Decode.int
