module Weather.Locations.UsCity exposing
    ( UsCity
    , decode
    , encode
    )

import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode
import Weather.Locations.UsCity.Id as UsCityId exposing (UsCityId(..))


type alias UsCity =
    { id : UsCityId
    , name : String
    , state : String
    }


encode : UsCity -> Encode.Value
encode city =
    Encode.object
        [ ( "id", UsCityId.encode city.id )
        , ( "name", Encode.string city.name )
        , ( "state", Encode.string city.name )
        ]


decode : Decode.Decoder UsCity
decode =
    Decode.succeed UsCity
        |> Pipeline.required "id" UsCityId.decode
        |> Pipeline.required "name" Decode.string
        |> Pipeline.required "state" Decode.string
