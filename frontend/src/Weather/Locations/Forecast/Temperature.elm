module Weather.Locations.Forecast.Temperature exposing
    ( Celsius(..)
    , Fahrenheit(..)
    , celsiusDecode
    , celsiusEncode
    , celsiusToFahrenheit
    , fahrenheitDecode
    , fahrenheitEncode
    , fahrenheitFromFloat
    , fahrenheitToCelsius
    , fahrenheitToFloat
    , fahrenheitToString
    )

import Json.Decode as Decode
import Json.Encode as Encode


type Fahrenheit
    = Fahrenheit Float


fahrenheitToFloat : Fahrenheit -> Float
fahrenheitToFloat (Fahrenheit float) =
    float


fahrenheitFromFloat : Float -> Fahrenheit
fahrenheitFromFloat =
    Fahrenheit


fahrenheitEncode : Fahrenheit -> Encode.Value
fahrenheitEncode =
    fahrenheitToFloat >> Encode.float


fahrenheitDecode : Decode.Decoder Fahrenheit
fahrenheitDecode =
    Decode.map fahrenheitFromFloat Decode.float


fahrenheitToString : Fahrenheit -> String
fahrenheitToString degrees =
    String.fromFloat (fahrenheitToFloat degrees) ++ " °F"


type Celsius
    = Celsius Float


celsiusToFloat : Celsius -> Float
celsiusToFloat (Celsius float) =
    float


celsiusFromFloat : Float -> Celsius
celsiusFromFloat =
    Celsius


celsiusEncode : Celsius -> Encode.Value
celsiusEncode =
    celsiusToFloat >> Encode.float


celsiusDecode : Decode.Decoder Celsius
celsiusDecode =
    Decode.map celsiusFromFloat Decode.float


fahrenheitToCelsius : Fahrenheit -> Celsius
fahrenheitToCelsius (Fahrenheit degrees) =
    Celsius ((degrees - 32) * (5 / 9))


celsiusToFahrenheit : Celsius -> Fahrenheit
celsiusToFahrenheit (Celsius degrees) =
    Fahrenheit (degrees * (9 / 5) + 32)
