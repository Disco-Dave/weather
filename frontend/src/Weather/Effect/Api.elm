module Weather.Effect.Api exposing
    ( Dependencies
    , Effect(..)
    , map
    , toCmd
    )

import Weather.Api.GetForecast as GetForecast
import Weather.Api.GetLocations as GetLocations
import Weather.Api.GetUsStates as GetUsStates
import Weather.Url.BasePath exposing (BasePath)


type alias Dependencies =
    { apiPath : BasePath
    }


type Effect msg
    = GetForecast (GetForecast.OnResponse msg) GetForecast.Request
    | GetLocations (GetLocations.OnResponse msg) GetLocations.Request
    | GetUsStates (GetUsStates.OnResponse msg)


map : (oldMsg -> newMsg) -> Effect oldMsg -> Effect newMsg
map f effect =
    let
        convert onResponse r =
            f (onResponse r)
    in
    case effect of
        GetForecast onResponse request ->
            GetForecast (convert onResponse) request

        GetLocations onResponse request ->
            GetLocations (convert onResponse) request

        GetUsStates onResponse ->
            GetUsStates (convert onResponse)


toCmd : Effect msg -> Dependencies -> Cmd msg
toCmd effect deps =
    case effect of
        GetForecast onResponse request ->
            GetForecast.get deps.apiPath onResponse request

        GetLocations onResponse request ->
            GetLocations.get deps.apiPath onResponse request

        GetUsStates onResponse ->
            GetUsStates.get deps.apiPath onResponse
