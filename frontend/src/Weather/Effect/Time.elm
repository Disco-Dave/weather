module Weather.Effect.Time exposing
    ( Effect(..)
    , map
    , toCmd
    )

import Task
import Time


type Effect msg
    = Now (Time.Posix -> msg)
    | Here (Time.Zone -> msg)


map : (oldMsg -> newMsg) -> Effect oldMsg -> Effect newMsg
map f effect =
    case effect of
        Now onPosix ->
            Now (\p -> f (onPosix p))

        Here onZone ->
            Here (\z -> f (onZone z))


toCmd : Effect msg -> Cmd msg
toCmd effect =
    case effect of
        Now onPosix ->
            Task.perform onPosix Time.now

        Here onZone ->
            Task.perform onZone Time.here
