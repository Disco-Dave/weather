import { Elm } from "./Main.elm";

const flags = {
  apiPath: process.env.WEATHER_FRONTEND_API_PATH ?? "/api/",
};

Elm.Main.init({ flags });
