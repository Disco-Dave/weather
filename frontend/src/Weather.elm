module Weather exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Browser
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attrs
import Html.Events as Events
import Html.Keyed
import Http
import Json.Decode as Decode
import Time
import Weather.Api.GetForecast as GetForecast
import Weather.Api.GetLocations as GetLocations
import Weather.Effect as Effect exposing (Effect)
import Weather.Effect.Api as Api
import Weather.Effect.Time as TimeEffect
import Weather.Locations.Forecast exposing (Forecast)
import Weather.Locations.Forecast.Temperature exposing (fahrenheitToString)
import Weather.Locations.UsCity exposing (UsCity)
import Weather.Locations.UsCity.Id as UsCityId exposing (UsCityId)
import Weather.Locations.UsCity.Name as UsCityName exposing (UsCityName)
import Weather.Locations.UsState.Abbreviation as UsStateAbbreviation exposing (UsStateAbbreviation)
import Weather.Utils.Http exposing (ErrorBody)
import Weather.Utils.Monad.Maybe exposing (isJust, isNothing)
import Weather.Utils.Monad.State as State
import Weather.Utils.Time exposing (viewHumanTime)


type alias CityForecast =
    { city : UsCity
    , forecast : Maybe Forecast
    }


type alias Model =
    { usCityName : String
    , usCityNameError : Maybe UsCityName.Error
    , usStateAbbreviation : String
    , usStateAbbreviationError : List UsStateAbbreviation.Error
    , isRequestActive : Bool
    , forecasts : Dict Int CityForecast
    , forecastsError : Maybe String
    , here : Time.Zone
    }


type Msg
    = UsCityNameChanged String
    | UsCityNameBlurred
    | UsStateAbbreviationChanged String
    | UsStateAbbreviationBlurred
    | SearchSubmitted
    | ResetRequested
    | ReceivedLocations (Result (ErrorBody GetLocations.BadResponse) GetLocations.Response)
    | ReceivedForecast UsCityId (Result Http.Error GetForecast.Response)
    | ReceivedZone Time.Zone


init : ( Model, Effect Msg )
init =
    ( { usCityName = ""
      , usCityNameError = Nothing
      , usStateAbbreviation = ""
      , usStateAbbreviationError = []
      , isRequestActive = False
      , forecastsError = Nothing
      , forecasts = Dict.empty
      , here = Time.utc
      }
    , Effect.Time (TimeEffect.Here ReceivedZone)
    )


parseUsCityName : Model -> ( Model, Maybe (Maybe UsCityName) )
parseUsCityName model =
    case UsCityName.fromString model.usCityName of
        Err UsCityName.Empty ->
            ( { model | usCityNameError = Nothing }
            , Just Nothing
            )

        Err error ->
            ( { model | usCityNameError = Just error }
            , Nothing
            )

        Ok value ->
            ( { model | usCityNameError = Nothing }
            , Just (Just value)
            )


parseUsStateAbbreviation : Model -> ( Model, Maybe (Maybe UsStateAbbreviation) )
parseUsStateAbbreviation model =
    case UsStateAbbreviation.fromString model.usStateAbbreviation of
        Err errors ->
            if List.any (\e -> e == UsStateAbbreviation.Empty) errors then
                ( { model | usStateAbbreviationError = [] }
                , Just Nothing
                )

            else
                ( { model | usStateAbbreviationError = errors }
                , Nothing
                )

        Ok value ->
            ( { model | usStateAbbreviationError = [] }
            , Just (Just value)
            )


parseGetLocationsRequest : Model -> ( Model, Maybe GetLocations.Request )
parseGetLocationsRequest =
    State.mapMaybe GetLocations.Request parseUsCityName
        |> State.applyMaybe parseUsStateAbbreviation


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        ReceivedZone zone ->
            ( { model | here = zone }
            , Effect.None
            )

        UsCityNameChanged value ->
            ( { model | usCityName = value }
            , Effect.None
            )

        UsCityNameBlurred ->
            ( parseUsCityName model
                |> Tuple.first
            , Effect.None
            )

        UsStateAbbreviationChanged value ->
            ( { model | usStateAbbreviation = value }
            , Effect.None
            )

        UsStateAbbreviationBlurred ->
            ( parseUsStateAbbreviation model
                |> Tuple.first
            , Effect.None
            )

        SearchSubmitted ->
            let
                ( updatedModel, maybeRequest ) =
                    parseGetLocationsRequest model

                effect =
                    case maybeRequest of
                        Nothing ->
                            Effect.None

                        Just request ->
                            request
                                |> Api.GetLocations ReceivedLocations
                                |> Effect.Api
            in
            ( updatedModel, effect )

        ResetRequested ->
            let
                ( initModel, _ ) =
                    init
            in
            ( { initModel | here = model.here }
            , Effect.None
            )

        ReceivedLocations response ->
            let
                nothingFound =
                    ( { model
                        | isRequestActive = False
                        , forecastsError = Just "Unable find any weather data."
                      }
                    , Effect.None
                    )
            in
            case response of
                Err _ ->
                    nothingFound

                Ok locations ->
                    if List.isEmpty locations then
                        nothingFound

                    else
                        ( { model
                            | isRequestActive = True
                            , forecastsError = Nothing
                            , forecasts =
                                locations
                                    |> List.map
                                        (\city ->
                                            ( UsCityId.toInt city.id
                                            , { city = city
                                              , forecast = Nothing
                                              }
                                            )
                                        )
                                    |> Dict.fromList
                          }
                        , locations
                            |> List.map (\c -> Api.GetForecast (ReceivedForecast c.id) c.id |> Effect.Api)
                            |> Effect.Batch
                        )

        ReceivedForecast cityId response ->
            case response of
                Err _ ->
                    ( { model
                        | isRequestActive = False
                        , forecastsError = Just "Unable find any weather data."
                      }
                    , Effect.None
                    )

                Ok forecast ->
                    let
                        updateForecast =
                            Maybe.map <| \cf -> { cf | forecast = Just forecast }

                        updatedCityForecasts =
                            Dict.update (UsCityId.toInt cityId) updateForecast model.forecasts

                        areAnyForecastsPending =
                            Dict.toList updatedCityForecasts
                                |> List.any (\( _, cf ) -> isNothing cf.forecast)
                    in
                    ( { model
                        | isRequestActive = areAnyForecastsPending
                        , forecasts = updatedCityForecasts
                      }
                    , Effect.None
                    )


type alias TextFieldInfo =
    { id : String
    , label : String
    , onBlur : Msg
    , onInput : String -> Msg
    , value : String
    , error : Maybe String
    , maxLength : Int
    }


viewTextField : TextFieldInfo -> Html Msg
viewTextField info =
    let
        isInvalid =
            isJust info.error
    in
    Html.div [ Attrs.classList [ ( "field", True ), ( "field--invalid", isInvalid ) ] ]
        [ Html.label [ Attrs.class "field__label", Attrs.for info.id ]
            [ Html.text info.label
            ]
        , Html.input
            [ Attrs.class "field__input"
            , Attrs.id info.id
            , Events.onBlur info.onBlur
            , Events.onInput info.onInput
            , Attrs.value info.value
            , Attrs.maxlength info.maxLength
            ]
            []
        , Html.p [ Attrs.class "field__feedback" ]
            [ Html.text (Maybe.withDefault "" info.error)
            ]
        ]


viewInfoField : { label : String, value : String } -> Html msg
viewInfoField info =
    Html.div [ Attrs.class "info" ]
        [ Html.span [ Attrs.class "info__label" ] [ Html.text info.label ]
        , Html.span [ Attrs.class "info__value" ] [ Html.text info.value ]
        ]


view : Model -> Browser.Document Msg
view model =
    let
        forecastData =
            Dict.toList model.forecasts
                |> List.filterMap (\( _, cf ) -> Maybe.map (\f -> ( cf.city.id, f )) cf.forecast)
                |> List.sortBy (\( _, f ) -> ( f.city, f.state ))
                |> List.map
                    (\( cityId, forecast ) ->
                        ( String.fromInt (UsCityId.toInt cityId)
                        , Html.li [ Attrs.class "forecast" ]
                            [ viewInfoField
                                { label = "Temperature"
                                , value = fahrenheitToString forecast.temperature
                                }
                            , viewInfoField
                                { label = "Condition"
                                , value = forecast.condition
                                }
                            , viewInfoField
                                { label = "City"
                                , value = forecast.city
                                }
                            , viewInfoField
                                { label = "State"
                                , value = forecast.state
                                }
                            , viewInfoField
                                { label = "Updated"
                                , value = viewHumanTime model.here forecast.lastUpdatedAt
                                }
                            ]
                        )
                    )
    in
    { title = "Weather"
    , body =
        [ Html.main_ [ Attrs.class "main" ]
            [ Html.h1 [ Attrs.class "header" ]
                [ Html.text "Search Location"
                ]
            , Html.form
                [ Attrs.class "form"
                , ( SearchSubmitted, True )
                    |> Decode.succeed
                    |> Events.preventDefaultOn "submit"
                ]
                [ viewTextField
                    { id = "city"
                    , label = "City"
                    , onBlur = UsCityNameBlurred
                    , onInput = UsCityNameChanged
                    , value = model.usCityName
                    , maxLength = 50
                    , error = Maybe.map UsCityName.errorString model.usCityNameError
                    }
                , viewTextField
                    { id = "state"
                    , label = "State Abbreviation"
                    , onBlur = UsStateAbbreviationBlurred
                    , onInput = UsStateAbbreviationChanged
                    , value = model.usStateAbbreviation
                    , maxLength = 2
                    , error =
                        case model.usStateAbbreviationError of
                            [] ->
                                Nothing

                            errors ->
                                errors
                                    |> List.map UsStateAbbreviation.errorString
                                    |> String.join " "
                                    |> Just
                    }
                , Html.button
                    [ Attrs.id "search-submit"
                    , Attrs.class "button"
                    , Attrs.type_ "submit"
                    ]
                    [ Html.text <| if model.isRequestActive then "Loading" else "Search" ]
                , Html.button
                    [ Attrs.id "search-reset"
                    , Attrs.class "button button--danger"
                    , Attrs.type_ "button"
                    , Events.onClick ResetRequested
                    ]
                    [ Html.text "Reset" ]
                ]
            , case model.forecastsError of
                Nothing ->
                    Html.text ""

                Just message ->
                    Html.p [ Attrs.class "search-error" ] [ Html.text message ]
            , Html.Keyed.ul
                [ Attrs.classList [ ( "forecasts", True ), ( "forecasts--loading", model.isRequestActive ) ] ]
                forecastData
            ]
        ]
    }
