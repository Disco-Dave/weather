module Main exposing (main)

import Browser
import Browser.Navigation as Navigation
import Html
import Url exposing (Url)
import Weather
import Weather.Effect as Effect
import Weather.Url.BasePath as BasePath


type alias Flags =
    { apiPath : String
    }


type alias Model =
    { navigationKey : Navigation.Key
    , url : Url
    , dependencies : Effect.Dependencies
    , weather : Weather.Model
    }


type Msg
    = UrlChanged Url
    | UrlRequested Browser.UrlRequest
    | WeatherMsg Weather.Msg


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url navigationKey =
    let
        ( weatherModel, weatherEffect ) =
            Weather.init

        dependencies =
            { apiPath = BasePath.fromString flags.apiPath
            }
    in
    ( { navigationKey = navigationKey
      , url = url
      , dependencies = dependencies
      , weather = weatherModel
      }
    , Effect.toCmd dependencies weatherEffect
        |> Cmd.map WeatherMsg
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Navigation.pushUrl model.navigationKey (Url.toString url) )

                Browser.External href ->
                    ( model, Navigation.load href )

        UrlChanged url ->
            ( { model | url = url }
            , Cmd.none
            )

        WeatherMsg weatherMsg ->
            let
                ( weatherModel, weatherEffect ) =
                    Weather.update weatherMsg model.weather
            in
            ( { model | weather = weatherModel }
            , Effect.toCmd model.dependencies weatherEffect
                |> Cmd.map WeatherMsg
            )


view : Model -> Browser.Document Msg
view model =
    let
        { title, body } =
            Weather.view model.weather
    in
    { title = title
    , body = List.map (Html.map WeatherMsg) body
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }
