const { createProxyMiddleware } = require("http-proxy-middleware");

const webapiPort = process.env.WEATHER_WEBAPI_HTTP_PORT ?? 3000;

module.exports = function (app) {
  app.use(
    createProxyMiddleware("/api", {
      target: `http://localhost:${webapiPort}/`,
      pathRewrite: {
        "^/api": "",
      },
    })
  );
};
