module Main (main) where

import Weather.Webapi qualified as Webapi
import Weather.Webapi.Config qualified as Config


main :: IO ()
main =
  Webapi.start =<< Config.fromEnvironmentVariables
