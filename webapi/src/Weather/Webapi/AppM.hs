module Weather.Webapi.AppM
  ( AppM (..)
  , runAppM
  , toHandler
  )
where

import Control.Exception.Annotated.UnliftIO (checkpoint)
import Control.Monad.Catch (MonadCatch, MonadThrow)
import Control.Monad.Except (ExceptT (ExceptT))
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader (MonadReader, ReaderT (runReaderT))
import Data.Coerce (coerce)
import GHC.Stack (HasCallStack)
import Katip qualified
import Servant qualified
import UnliftIO (MonadUnliftIO)
import UnliftIO.Exception (try)
import Weather.Shared.Exception.Annotations qualified as Annotations
import Weather.Shared.Logging (ReaderWithLoggingConfig (..))
import Weather.Webapi.AppData (AppData (..))


newtype AppM a = AppM (ReaderT AppData IO a)
  deriving
    ( Functor
    , Applicative
    , Monad
    , MonadFail
    , MonadIO
    , MonadUnliftIO
    , MonadThrow
    , MonadCatch
    , MonadReader AppData
    )
  deriving
    ( Katip.Katip
    , Katip.KatipContext
    )
    via ReaderWithLoggingConfig AppM AppData


runAppM :: HasCallStack => AppData -> AppM value -> IO value
runAppM appData app =
  let (AppM modifiedApp) = checkpoint Annotations.appMonadWasSetup app
   in runReaderT modifiedApp appData


toHandler :: HasCallStack => AppData -> AppM value -> Servant.Handler value
toHandler appData =
  coerce . try @_ @Servant.ServerError . runAppM appData
