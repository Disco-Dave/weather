module Weather.Webapi.Http
  ( makeWaiApplication
  , start
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (when)
import Control.Monad.Reader (ask)
import Data.Function ((&))
import Data.Proxy (Proxy (Proxy))
import GHC.Stack (HasCallStack)
import Katip qualified
import Katip.Wai qualified
import Network.Wai qualified as Wai
import Network.Wai.Handler.Warp qualified as Warp
import Servant (hoistServer, serve)
import UnliftIO (MonadUnliftIO (withRunInIO))
import Weather.Webapi.AppM (AppM)
import Weather.Webapi.AppM qualified as AppM
import Weather.Webapi.Config (HttpConfig (..))
import Weather.Webapi.Http.Middleware qualified as Middleware
import Weather.Webapi.Http.Routes qualified as Routes


makeWaiApplication :: HasCallStack => Bool -> AppM Wai.Application
makeWaiApplication enableSwagger = do
  appData <- ask

  let toApplication api apiServer =
        let app request send =
              let hoistedApp =
                    let hoistedServer = hoistServer api (AppM.toHandler appData) apiServer
                     in serve api hoistedServer
               in withRunInIO $ \runInIO -> hoistedApp request (runInIO . send)
         in Middleware.apply app

  pure . Katip.Wai.runApplication (AppM.runAppM appData) $
    if enableSwagger
      then toApplication (Proxy @Routes.ApiWithSwagger) Routes.serverWithSwagger
      else toApplication (Proxy @Routes.Api) Routes.server


start :: HasCallStack => HttpConfig -> AppM ()
start config =
  checkpointCallStack . Katip.katipAddNamespace "http" $ do
    application <- makeWaiApplication config.enableSwagger

    withRunInIO $ \runInIO ->
      let beforeMainLoop = do
            let uri = "http://localhost:" <> Katip.ls (show config.port)

            runInIO . Katip.logLocM Katip.InfoS $ "Server running at " <> uri

            when config.enableSwagger $
              runInIO . Katip.logLocM Katip.InfoS $
                "Visit Swagger at " <> uri <> "/docs"

          settings =
            Warp.defaultSettings
              & Warp.setBeforeMainLoop beforeMainLoop
              & Warp.setPort config.port
              & Warp.setOnException (\_ _ -> pure ()) -- omit logging because middleware should have done this already
       in Warp.runSettings settings application
