module Weather.Webapi.Http.OpenApi
  ( DocumentedBy (..)
  )
where

import Data.Aeson qualified as Aeson
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Data.Typeable (Typeable)
import Servant (FromHttpApiData, ToHttpApiData)


newtype DocumentedBy docs a = DocumentedBy {unwrap :: a}
  deriving
    ( Aeson.ToJSON
    , Aeson.FromJSON
    , ToHttpApiData
    , FromHttpApiData
    , Show
    , Eq
    )


instance (OpenApi.ToSchema docs, Typeable a) => OpenApi.ToSchema (DocumentedBy docs a) where
  declareNamedSchema _ =
    OpenApi.declareNamedSchema @docs Proxy


instance (OpenApi.ToParamSchema docs) => OpenApi.ToParamSchema (DocumentedBy docs a) where
  toParamSchema _ =
    OpenApi.toParamSchema @docs Proxy
