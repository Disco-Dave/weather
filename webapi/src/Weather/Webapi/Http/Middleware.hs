module Weather.Webapi.Http.Middleware
  ( unliftApplication
  , liftMiddleware
  , apply
  )
where

import Control.Monad.IO.Class (liftIO)
import Data.Coerce (coerce)
import Data.Monoid (Endo (..))
import GHC.Stack (HasCallStack)
import Katip qualified
import Katip.Wai (ApplicationT, MiddlewareT)
import Katip.Wai qualified
import Network.Wai (Application, Middleware)
import UnliftIO (MonadUnliftIO (withRunInIO))
import Weather.Shared.Logging (withExceptionLogging)
import Weather.Webapi.AppM (AppM (..))


unliftApplication :: ApplicationT AppM -> AppM Application
unliftApplication application = do
  withRunInIO $ \toIO ->
    pure $ \req send -> toIO $ application req (liftIO . send)


liftMiddleware :: Middleware -> MiddlewareT AppM
liftMiddleware middleware application request send = do
  unliftedApplication <- unliftApplication application
  withRunInIO $ \toIO ->
    middleware unliftedApplication request (toIO . send)


logRequestsAndResponses :: MiddlewareT AppM
logRequestsAndResponses =
  Katip.Wai.middleware Katip.InfoS


logExceptions :: HasCallStack => MiddlewareT AppM
logExceptions application request send =
  withExceptionLogging Katip.CriticalS $
    application request send


apply :: HasCallStack => MiddlewareT AppM
apply =
  appEndo . mconcat . coerce $
    -- middelware runs in the same order they are declared in this list.
    [ logRequestsAndResponses
    , logExceptions
    ]
