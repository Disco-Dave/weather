module Weather.Webapi.Http.Routes
  ( Api
  , ApiWithSwagger
  , server
  , serverWithSwagger
  )
where

import Control.Lens ((.~), (?~))
import Data.Function ((&))
import Data.OpenApi (OpenApi)
import Data.OpenApi qualified as OpenApi
import Data.OpenApi.Operation (applyTagsFor)
import Data.Text qualified as Text
import Data.Version (showVersion)
import GHC.Stack (HasCallStack)
import Paths_weather_webapi (version)
import Servant (HasServer (ServerT), Proxy (Proxy), type (:<|>) (..), type (:>))
import Servant.OpenApi (subOperations, toOpenApi)
import Servant.Swagger.UI (SwaggerSchemaUI, swaggerSchemaUIServerT)
import Weather.Webapi.AppM (AppM)

import Weather.Webapi.HealthChecks.Http qualified as HealthChecks
import Weather.Webapi.Locations.Http qualified as Locations


type HealthChecks = "health-checks" :> HealthChecks.Api
type Locations = "locations" :> Locations.Api


type Api =
  HealthChecks
    :<|> Locations


server :: HasCallStack => ServerT Api AppM
server =
  HealthChecks.server
    :<|> Locations.server


type ApiWithSwagger =
  SwaggerSchemaUI "docs" "openapi.json" :<|> Api


openApi :: OpenApi
openApi =
  let tagRoutes proxy name description =
        applyTagsFor
          (subOperations @_ @Api proxy Proxy)
          [name & OpenApi.description ?~ description]
   in toOpenApi @Api Proxy
        & OpenApi.info . OpenApi.title .~ "weather-webapi"
        & OpenApi.info . OpenApi.description ?~ "Public REST API used by the weather front end"
        & OpenApi.info . OpenApi.version .~ Text.pack (showVersion version)
        & tagRoutes (Proxy @HealthChecks) "Health Checks" "End points for checking the health of the REST API"
        & tagRoutes (Proxy @Locations) "Locations" "End points for getting locations and weather."


serverWithSwagger :: HasCallStack => ServerT ApiWithSwagger AppM
serverWithSwagger =
  swaggerSchemaUIServerT openApi :<|> server
