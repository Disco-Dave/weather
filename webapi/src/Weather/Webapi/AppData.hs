module Weather.Webapi.AppData
  ( AppData (..)
  , withAppData
  )
where

import Control.Monad.Cont (ContT (..))
import Data.Has (Has (..))
import Data.Pool (Pool)
import Database.PostgreSQL.Simple qualified as Postgres
import GHC.Stack (HasCallStack)
import Katip.Monadic (KatipContextTState)
import Weather.Shared.Database (withConnectionPool)
import Weather.Shared.Logging (withLoggingData)
import Weather.Webapi.Config (Config)
import Weather.Webapi.Config qualified as Config


data AppData = AppData
  { logging :: KatipContextTState
  , connectionPool :: Pool Postgres.Connection
  }


instance Has KatipContextTState AppData where
  getter = (.logging)
  modifier f original =
    let updated = f original.logging
     in original{logging = updated}


instance Has (Pool Postgres.Connection) AppData where
  getter = (.connectionPool)
  modifier f original =
    let updated = f original.connectionPool
     in original{connectionPool = updated}


withAppData :: HasCallStack => Config -> (AppData -> IO a) -> IO a
withAppData config =
  runContT $ do
    logging <- ContT $ withLoggingData "weather-webapi" () config.logging
    connectionPool <- ContT $ withConnectionPool config.database
    pure AppData{..}
