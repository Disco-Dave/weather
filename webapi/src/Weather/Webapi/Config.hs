module Weather.Webapi.Config
  ( HttpConfig (..)
  , Config (..)
  , parser
  , fromEnvironmentVariables
  )
where

import Env qualified
import Network.Wai.Handler.Warp qualified as Warp
import Weather.Shared.Database (DatabaseConfig)
import Weather.Shared.EnvParsers (bool, databaseConfig, loggingConfig)
import Weather.Shared.Logging (LoggingConfig)


data HttpConfig = HttpConfig
  { port :: Warp.Port
  , enableSwagger :: Bool
  }


httpConfig :: Env.Parser Env.Error HttpConfig
httpConfig =
  Env.prefixed "HTTP_" $
    HttpConfig
      <$> Env.var Env.auto "PORT" (Env.help "Port to run the server on. Defaults to 3000." <> Env.def 3000)
      <*> Env.var bool "ENABLE_SWAGGER" (Env.help "Enable swagger at /docs. Defaults to false." <> Env.def False)


data Config = Config
  { http :: HttpConfig
  , database :: DatabaseConfig
  , logging :: LoggingConfig
  }


parser :: Env.Parser Env.Error Config
parser =
  Env.prefixed "WEATHER_WEBAPI_" $
    Config
      <$> httpConfig
      <*> databaseConfig
      <*> loggingConfig


fromEnvironmentVariables :: IO Config
fromEnvironmentVariables =
  Env.parse (Env.header "weather-webapi") parser
