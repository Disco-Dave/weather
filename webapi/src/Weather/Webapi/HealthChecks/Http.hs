module Weather.Webapi.HealthChecks.Http
  ( GetIndex
  , getIndex
  , GetDatabases
  , getDatabases
  , Api
  , server
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.SqlQQ (sql)
import GHC.Stack (HasCallStack)
import Servant
  ( Description
  , GetNoContent
  , HasServer (ServerT)
  , NoContent (NoContent)
  , Summary
  , type (:<|>) ((:<|>))
  , type (:>)
  )
import Weather.Shared.Database (withConnection)
import Weather.Webapi.AppM (AppM)


type GetIndex =
  Summary "Is this API reachable?"
    :> Description "Allows you to check if the API is reachable. Hardcoded to return 204."
    :> GetNoContent


getIndex :: HasCallStack => AppM NoContent
getIndex =
  checkpointCallStack $ pure NoContent


type GetDatabases =
  Summary "Can the API communicate with our database?"
    :> Description "Allows you to check if the API can communicate with the database. Hardcoded to return 204."
    :> "databases"
    :> GetNoContent


getDatabases :: HasCallStack => AppM NoContent
getDatabases =
  checkpointCallStack $ do
    _ <-
      withConnection $ \connection ->
        Postgres.query_ @(Postgres.Only Int) connection [sql| SELECT 1 |]

    pure NoContent


type Api =
  GetIndex :<|> GetDatabases


server :: HasCallStack => ServerT Api AppM
server =
  getIndex :<|> getDatabases
