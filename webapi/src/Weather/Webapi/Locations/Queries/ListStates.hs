module Weather.Webapi.Locations.Queries.ListStates
  ( UsState (..)
  , query
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (unless)
import Data.Aeson qualified as Aeson
import Data.Bifunctor (Bifunctor (..))
import Data.Either (partitionEithers)
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NonEmpty
import Data.OpenApi qualified as OpenApi
import Data.Text (Text)
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.FromRow (RowParser)
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Env.Generic (Generic)
import GHC.Stack (HasCallStack)
import Katip qualified
import Validation (Validation (..), validationToEither)
import Weather.Shared.Database (fieldByName, withConnection)
import Weather.Webapi.AppM (AppM)
import Weather.Webapi.Locations.UsState.Abbreviation (UsStateAbbreviation)
import Weather.Webapi.Locations.UsState.Abbreviation qualified as UsStateAbbreviation
import Weather.Webapi.Locations.UsState.Name (UsStateName)
import Weather.Webapi.Locations.UsState.Name qualified as UsStateName


data QueryRow = QueryRow
  { abbreviation :: Text
  , name :: Text
  }
  deriving (Show, Eq)


queryRowParser :: RowParser QueryRow
queryRowParser = do
  abbreviation <- fieldByName "abbreviation"
  name <- fieldByName "name"
  pure QueryRow{..}


data UsState = UsState
  { abbreviation :: UsStateAbbreviation
  , name :: UsStateName
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON UsState
instance OpenApi.ToSchema UsState


parseUsState :: QueryRow -> Either (NonEmpty Text) UsState
parseUsState row =
  validationToEither $ do
    let abbreviation =
          case UsStateAbbreviation.fromText row.abbreviation of
            Right value ->
              Success value
            Left err ->
              Failure $ fmap UsStateAbbreviation.errorText err

        name =
          case UsStateName.fromText row.name of
            Right value ->
              Success value
            Left err ->
              Failure . NonEmpty.singleton $ UsStateName.errorText err
     in UsState
          <$> abbreviation
          <*> name


query :: HasCallStack => AppM [UsState]
query =
  checkpointCallStack $ do
    let sqlQuery =
          [sql|
            SELECT
              s.abbreviation,
              s.name
            FROM public.us_states AS s;
          |]

    rows <-
      withConnection $ \connection ->
        Postgres.queryWith_ queryRowParser connection sqlQuery

    let (parseErrors, usStates) =
          partitionEithers $
            fmap (\r -> first (r.abbreviation,) $ parseUsState r) rows

    unless (null parseErrors) $ do
      let katipContext =
            let convertError (abbreviation, errors) =
                  Katip.sl "abbreviation" abbreviation <> Katip.sl "errors" errors
             in Katip.sl "malformedRows" $ fmap convertError parseErrors

      Katip.katipAddContext katipContext $ do
        Katip.logLocM Katip.ErrorS "Malformed row(s) detected!"

    pure usStates
