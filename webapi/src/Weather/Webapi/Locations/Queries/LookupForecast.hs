module Weather.Webapi.Locations.Queries.LookupForecast
  ( Forecast (..)
  , query
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (unless)
import Data.Aeson qualified as Aeson
import Data.Bifunctor (Bifunctor (..))
import Data.Either (partitionEithers)
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (listToMaybe)
import Data.OpenApi qualified as OpenApi
import Data.Text (Text)
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.FromRow (RowParser)
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Env.Generic (Generic)
import GHC.Stack (HasCallStack)
import Katip qualified
import Validation (Validation (..), validationToEither)
import Weather.Shared.Database (fieldByName, withConnection)
import Weather.Webapi.AppM (AppM)
import Weather.Webapi.Locations.Forecast.Condition (Condition)
import Weather.Webapi.Locations.Forecast.Condition qualified as Condition
import Weather.Webapi.Locations.Forecast.LastUpdatedAt (LastUpdatedAt)
import Weather.Webapi.Locations.Forecast.Temperature (Fahrenheit)
import Weather.Webapi.Locations.UsCity.Id (UsCityId)
import Weather.Webapi.Locations.UsCity.Name (UsCityName)
import Weather.Webapi.Locations.UsCity.Name qualified as UsCityName
import Weather.Webapi.Locations.UsState.Name (UsStateName)
import Weather.Webapi.Locations.UsState.Name qualified as UsStateName


data QueryRow = QueryRow
  { weatherId :: Integer
  , city :: Text
  , state :: Text
  , temperature :: Fahrenheit
  , condition :: Text
  , lastUpdatedAt :: LastUpdatedAt
  }
  deriving (Show, Eq)


queryRowParser :: RowParser QueryRow
queryRowParser = do
  weatherId <- fieldByName "id"
  city <- fieldByName "city_name"
  state <- fieldByName "state_name"
  temperature <- fieldByName "temperature"
  condition <- fieldByName "condition"
  lastUpdatedAt <- fieldByName "last_updated_at"
  pure QueryRow{..}


data Forecast = Forecast
  { city :: UsCityName
  , state :: UsStateName
  , temperature :: Fahrenheit
  , condition :: Condition
  , lastUpdatedAt :: LastUpdatedAt
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON Forecast
instance OpenApi.ToSchema Forecast


parseForecast :: QueryRow -> Either (NonEmpty Text) Forecast
parseForecast row =
  validationToEither $ do
    let city =
          case UsCityName.fromText row.city of
            Right value ->
              Success value
            Left err ->
              Failure . NonEmpty.singleton $ UsCityName.errorText err

        state =
          case UsStateName.fromText row.state of
            Right value ->
              Success value
            Left err ->
              Failure . NonEmpty.singleton $ UsStateName.errorText err

        condition =
          case Condition.fromText row.condition of
            Right value ->
              Success value
            Left err ->
              Failure . NonEmpty.singleton $ Condition.errorText err
     in Forecast
          <$> city
          <*> state
          <*> pure row.temperature
          <*> condition
          <*> pure row.lastUpdatedAt


query :: HasCallStack => UsCityId -> AppM (Maybe Forecast)
query cityId =
  checkpointCallStack $ do
    let sqlParams = Postgres.Only cityId

        sqlQuery =
          [sql|
            SELECT
              f.id AS id,
              c.name AS city_name,
              s.name AS state_name,
              f.temperature AS temperature,
              f.condition AS condition,
              f.updated_at AS last_updated_at
            FROM public.forecasts AS f
              INNER JOIN public.us_cities AS c
                ON c.id = f.city_id
              INNER JOIN public.us_states AS s
                ON s.abbreviation = c.state
            WHERE c.id = ?;
          |]

    rows <-
      withConnection $ \connection ->
        Postgres.queryWith queryRowParser connection sqlQuery sqlParams

    let (parseErrors, weather) =
          partitionEithers $
            fmap (\r -> first (r.weatherId,) $ parseForecast r) rows

    unless (null parseErrors) $ do
      let katipContext =
            let convertError (weatherId, errors) =
                  Katip.sl "weatherId" weatherId <> Katip.sl "errors" errors
             in Katip.sl "malformedRows" $ fmap convertError parseErrors

      Katip.katipAddContext katipContext $ do
        Katip.logLocM Katip.ErrorS "Malformed row(s) detected!"

    pure $ listToMaybe weather
