module Weather.Webapi.Locations.Queries.FindCities
  ( QueryParams (..)
  , City (..)
  , query
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (unless)
import Control.Monad.ST.Strict (runST)
import Data.Aeson qualified as Aeson
import Data.Bifunctor (Bifunctor (..))
import Data.Either (partitionEithers)
import Data.Foldable (Foldable (toList))
import Data.List.NonEmpty (NonEmpty)
import Data.List.NonEmpty qualified as NonEmpty
import Data.OpenApi qualified as OpenApi
import Data.STRef.Strict qualified as STRef
import Data.Sequence ((|>))
import Data.Sequence qualified as Seq
import Data.Text (Text)
import Data.Text qualified as Text
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.FromRow (RowParser)
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Database.PostgreSQL.Simple.ToField (ToField (toField))
import Database.PostgreSQL.Simple.ToField qualified as FromField
import Env.Generic (Generic)
import GHC.Stack (HasCallStack)
import Katip qualified
import Validation (Validation (..), validationToEither)
import Weather.Shared.Database (fieldByName, withConnection)
import Weather.Webapi.AppM (AppM)
import Weather.Webapi.Locations.UsCity.Id (UsCityId)
import Weather.Webapi.Locations.UsCity.Name (UsCityName)
import Weather.Webapi.Locations.UsCity.Name qualified as UsCityName
import Weather.Webapi.Locations.UsState.Abbreviation (UsStateAbbreviation)
import Weather.Webapi.Locations.UsState.Abbreviation qualified as UsStateAbbreviation


data QueryParams = QueryParams
  { name :: Maybe UsCityName
  , state :: Maybe UsStateAbbreviation
  }
  deriving (Show, Eq)


data QueryRow = QueryRow
  { id :: UsCityId
  , name :: Text
  , state :: Text
  }
  deriving (Show, Eq)


data City = City
  { id :: UsCityId
  , name :: UsCityName
  , state :: UsStateAbbreviation
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON City
instance OpenApi.ToSchema City


buildQuery :: QueryParams -> (Postgres.Query, [FromField.Action])
buildQuery queryParams =
  let (filters, sqlParams) = runST $ do
        sqlParamsRef <- STRef.newSTRef $ Seq.fromList []
        filtersRef <- STRef.newSTRef [sql||]

        case queryParams.name of
          Nothing -> pure ()
          Just name -> do
            let lowerName = Text.toLower $ UsCityName.toText name

            STRef.modifySTRef'
              sqlParamsRef
              (|> toField lowerName)

            STRef.modifySTRef
              filtersRef
              (<> " AND lower(c.name) LIKE concat('%', ?, '%')")

        case queryParams.state of
          Nothing -> pure ()
          Just state -> do
            STRef.modifySTRef'
              sqlParamsRef
              (|> toField state)

            STRef.modifySTRef
              filtersRef
              (<> " AND c.state = ?")

        finalFilters <- STRef.readSTRef filtersRef
        finalSqlParams <- toList <$> STRef.readSTRef sqlParamsRef

        pure (finalFilters, finalSqlParams)

      selectClause =
        [sql|
            SELECT 
              c.id,
              c.name,
              c.state
          |]

      fromClause =
        [sql|
            FROM public.us_cities AS c
          |]

      whereClause =
        "WHERE 1 = 1 " <> filters

      sqlQuery = selectClause <> " " <> fromClause <> " " <> whereClause <> ";"
   in (sqlQuery, sqlParams)


queryRowParser :: RowParser QueryRow
queryRowParser = do
  cityId <- fieldByName "id"
  name <- fieldByName "name"
  state <- fieldByName "state"
  pure QueryRow{id = cityId, ..}


parseCity :: QueryRow -> Either (NonEmpty Text) City
parseCity row =
  validationToEither $ do
    let name =
          case UsCityName.fromText row.name of
            Right value ->
              Success value
            Left err ->
              Failure . NonEmpty.singleton $ UsCityName.errorText err

        state =
          case UsStateAbbreviation.fromText row.state of
            Right value ->
              Success value
            Left errors ->
              Failure $ fmap UsStateAbbreviation.errorText errors
     in City row.id <$> name <*> state


query :: HasCallStack => QueryParams -> AppM [City]
query queryParams =
  checkpointCallStack $ do
    let (sqlQuery, sqlParams) = buildQuery queryParams

    rows <-
      withConnection $ \connection ->
        Postgres.queryWith queryRowParser connection sqlQuery sqlParams

    let (parseErrors, cities) =
          partitionEithers $
            fmap (\r -> first (r.id,) $ parseCity r) rows

    unless (null parseErrors) $ do
      let katipContext =
            let convertError (cityId, errors) =
                  Katip.sl "cityId" cityId <> Katip.sl "errors" errors
             in Katip.sl "malformedRows" $ fmap convertError parseErrors

      Katip.katipAddContext katipContext $ do
        Katip.logLocM Katip.ErrorS "Malformed row(s) detected!"

    pure cities
