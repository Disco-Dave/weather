module Weather.Webapi.Locations.Forecast.Condition
  ( Condition
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import Data.Text qualified as Text
import Database.PostgreSQL.Simple.ToField (ToField)


newtype Condition = Condition Text
  deriving (Show, Eq, Ord, Aeson.ToJSON, ToField)


data Error
  = Empty
  | LongerThan100Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


errorText :: Error -> Text
errorText = \case
  Empty -> "Weather condition is empty."
  LongerThan100Characters -> "Weather condition is longer than 100 characters."


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "ForecastConditionError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "ForecastConditionError"
        & OpenApi.description ?~ "Error describing why a weather condition was rejected."
        & OpenApi.example ?~ Aeson.toJSON (minBound @Error)
        & OpenApi.enum_ ?~ fmap Aeson.toJSON [minBound @Error ..]


fromText :: Text -> Either Error Condition
fromText rawText
  | Text.null normalizedText = Left Empty
  | Text.length normalizedText > 100 = Left LongerThan100Characters
  | otherwise = Right $ Condition normalizedText
 where
  normalizedText = Text.strip rawText


toText :: Condition -> Text
toText =
  coerce


instance OpenApi.ToSchema Condition where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "ForecastCondition") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "ForecastCondition"
        & OpenApi.description ?~ "Condition of the weather."
        & OpenApi.example ?~ Aeson.toJSON @Text "Cloudy"
