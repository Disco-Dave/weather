module Weather.Webapi.Locations.Forecast.Temperature
  ( Fahrenheit (..)
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Database.PostgreSQL.Simple.FromField (FromField)
import Database.PostgreSQL.Simple.ToField (ToField)


newtype Fahrenheit = Fahrenheit {toDouble :: Double}
  deriving
    ( Show
    , Eq
    , Ord
    , Num
    , Fractional
    , Enum
    , Real
    , Aeson.ToJSON
    , Aeson.FromJSON
    , ToField
    , FromField
    )


instance OpenApi.ToSchema Fahrenheit where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "Fahrenheit") $
      OpenApi.toSchema @Double Proxy
        & OpenApi.title ?~ "Fahrenheit"
        & OpenApi.description ?~ "Temperature in Fahrenheit"
        & OpenApi.example ?~ Aeson.toJSON @Fahrenheit 68.3
