module Weather.Webapi.Locations.Forecast.LastUpdatedAt
  ( LastUpdatedAt (..)
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Data.Time (UTCTime)
import Database.PostgreSQL.Simple.FromField (FromField)
import Database.PostgreSQL.Simple.ToField (ToField)


newtype LastUpdatedAt = LastUpdatedAt {toUTCTime :: UTCTime}
  deriving
    ( Show
    , Eq
    , Ord
    , Aeson.ToJSON
    , Aeson.FromJSON
    , ToField
    , FromField
    )


instance OpenApi.ToSchema LastUpdatedAt where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "ForecastLastUpdatedAt") $
      OpenApi.toSchema @UTCTime Proxy
        & OpenApi.title ?~ "ForecastLastUpdatedAt"
        & OpenApi.description ?~ "UTC timestamp of when the weather was last updated."
