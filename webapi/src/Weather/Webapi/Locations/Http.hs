module Weather.Webapi.Locations.Http
  ( Api
  , server
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Data.Coerce (coerce)
import Data.Text (Text)
import GHC.Stack (HasCallStack)
import Servant
  ( Capture
  , Description
  , Get
  , HasServer (ServerT)
  , JSON
  , QueryParam
  , StdMethod (..)
  , Summary
  , UVerb
  , Union
  , WithStatus (WithStatus)
  , respond
  , (:<|>)
  , type (:<|>) (..)
  , type (:>)
  )
import Weather.Webapi.AppM (AppM)
import Weather.Webapi.Http.OpenApi (DocumentedBy (..))
import Weather.Webapi.Locations.Http.Requests.GetIndex
  ( GetIndexBadRequest
  , GetIndexRequest (GetIndexRequest)
  )
import Weather.Webapi.Locations.Http.Requests.GetIndex qualified as GetIndexRequest
import Weather.Webapi.Locations.Queries.FindCities qualified as FindCities
import Weather.Webapi.Locations.Queries.ListStates qualified as ListStates
import Weather.Webapi.Locations.Queries.LookupForecast qualified as LookupForecast
import Weather.Webapi.Locations.UsCity.Id (UsCityId)
import Weather.Webapi.Locations.UsCity.Name (UsCityName)
import Weather.Webapi.Locations.UsState.Abbreviation (UsStateAbbreviation)


type GetIndexResponse =
  '[WithStatus 200 [FindCities.City], WithStatus 400 GetIndexBadRequest]


type GetIndex =
  Summary "List of available cities that have weather data."
    :> Description "Currently a list of top 10 cities by population in the United States."
    :> QueryParam "city" (DocumentedBy UsCityName Text)
    :> QueryParam "state" (DocumentedBy UsStateAbbreviation Text)
    :> UVerb 'GET '[JSON] GetIndexResponse


getIndex
  :: HasCallStack
  => Maybe (DocumentedBy UsCityName Text)
  -> Maybe (DocumentedBy UsStateAbbreviation Text)
  -> AppM (Union GetIndexResponse)
getIndex maybeCity maybeState =
  checkpointCallStack $ do
    let queryParamsResult =
          GetIndexRequest.toFindCitiesQueryParams $
            GetIndexRequest
              { city = coerce @_ @(Maybe Text) maybeCity
              , state = coerce @_ @(Maybe Text) maybeState
              }

    case queryParamsResult of
      Left badRequest ->
        respond (WithStatus @400 badRequest)
      Right queryParams -> do
        cities <- FindCities.query queryParams
        respond (WithStatus @200 cities)


type GetStates =
  Summary "List all of the US states and terrorities."
    :> "states"
    :> Get '[JSON] [ListStates.UsState]


getStates :: AppM [ListStates.UsState]
getStates =
  checkpointCallStack
    ListStates.query


type GetForecastResponse =
  '[WithStatus 200 LookupForecast.Forecast, WithStatus 404 Text]


type GetForecast =
  Summary "Get current weather forecast for a city"
    :> Capture "cityId" UsCityId
    :> "weather"
    :> UVerb 'GET '[JSON] GetForecastResponse


getWeather :: UsCityId -> AppM (Union GetForecastResponse)
getWeather cityId =
  checkpointCallStack $ do
    result <- LookupForecast.query cityId
    case result of
      Nothing ->
        respond (WithStatus @404 ("No weather data found." :: Text))
      Just weather ->
        respond (WithStatus @200 weather)


type Api =
  GetIndex
    :<|> GetStates
    :<|> GetForecast


server :: HasCallStack => ServerT Api AppM
server =
  getIndex
    :<|> getStates
    :<|> getWeather
