module Weather.Webapi.Locations.UsState.Abbreviation
  ( UsStateAbbreviation
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Char qualified as Char
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import Data.Maybe (catMaybes)
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Database.PostgreSQL.Simple.ToField


newtype UsStateAbbreviation = UsStateAbbreviation Text
  deriving (Show, Eq, Ord, Aeson.ToJSON, ToField)


data Error
  = Empty
  | NotExactlyTwoLetters
  | ContainsNonLetters
  deriving (Show, Eq, Ord, Enum, Bounded)


errorText :: Error -> Text
errorText = \case
  Empty -> "US state abbreviation is empty."
  NotExactlyTwoLetters -> "US state abbreviation is not exactly two letters in length."
  ContainsNonLetters -> "US state abbreviation contains non-letters."


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsStateAbbreviationError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "UsStateAbbreviationError"
        & OpenApi.description ?~ "Error describing why a state abbreviation was rejected."
        & OpenApi.example ?~ Aeson.toJSON (minBound @Error)
        & OpenApi.enum_ ?~ fmap Aeson.toJSON [minBound @Error ..]


fromText :: Text -> Either (NonEmpty Error) UsStateAbbreviation
fromText text =
  let normalizedText = Text.toUpper $ Text.strip text

      emptyCheck =
        if Text.null normalizedText
          then Just Empty
          else Nothing

      notExactlyTwoLettersCheck =
        if Text.length normalizedText /= 2
          then Just NotExactlyTwoLetters
          else Nothing

      containsNonLettersCheck =
        if Text.all Char.isLetter normalizedText
          then Nothing
          else Just ContainsNonLetters

      allChecks =
        catMaybes
          [ emptyCheck
          , notExactlyTwoLettersCheck
          , containsNonLettersCheck
          ]
   in case nonEmpty allChecks of
        Just errors -> Left errors
        Nothing -> Right $ UsStateAbbreviation normalizedText


toText :: UsStateAbbreviation -> Text
toText =
  coerce


instance OpenApi.ToParamSchema UsStateAbbreviation where
  toParamSchema _ = do
    OpenApi.toSchema @Text Proxy
      & OpenApi.title ?~ "UsStateAbbreviation"
      & OpenApi.description ?~ "US postal abbreviation of a US state or territory."
      & OpenApi.example ?~ Aeson.toJSON @Text "IL"


instance OpenApi.ToSchema UsStateAbbreviation where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsStateAbbreviation") $
      OpenApi.toParamSchema @UsStateAbbreviation Proxy
