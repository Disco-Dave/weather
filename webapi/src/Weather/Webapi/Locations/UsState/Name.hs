module Weather.Webapi.Locations.UsState.Name
  ( UsStateName
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Data (Proxy (Proxy))
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Text (Text)
import Data.Text qualified as Text


newtype UsStateName = UsStateName Text
  deriving (Show, Eq, Ord, Aeson.ToJSON)


data Error
  = Empty
  | LongerThan50Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


errorText :: Error -> Text
errorText = \case
  Empty -> "US state name is empty."
  LongerThan50Characters -> "US state name is longer than 50 characters."


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsStateNameError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "UsStateNameError"
        & OpenApi.description ?~ "Error describing why a state name was rejected."
        & OpenApi.example ?~ Aeson.toJSON (minBound @Error)
        & OpenApi.enum_ ?~ fmap Aeson.toJSON [minBound @Error ..]


fromText :: Text -> Either Error UsStateName
fromText rawText
  | Text.null normalizedText = Left Empty
  | Text.length normalizedText > 50 = Left LongerThan50Characters
  | otherwise = Right $ UsStateName normalizedText
 where
  normalizedText = Text.strip rawText


toText :: UsStateName -> Text
toText =
  coerce


instance OpenApi.ToSchema UsStateName where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsStateName") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "UsStateName"
        & OpenApi.description ?~ "Name of a US state or territory."
        & OpenApi.example ?~ Aeson.toJSON @Text "Illinois"
