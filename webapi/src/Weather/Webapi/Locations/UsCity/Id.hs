module Weather.Webapi.Locations.UsCity.Id
  ( UsCityId (..)
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Database.PostgreSQL.Simple.FromField (FromField)
import Database.PostgreSQL.Simple.ToField (ToField)
import Servant qualified


newtype UsCityId = UsCityId {toInteger :: Integer}
  deriving
    ( Show
    , Eq
    , Ord
    , Num
    , Enum
    , Real
    , Integral
    , Aeson.ToJSON
    , Aeson.FromJSON
    , Servant.ToHttpApiData
    , Servant.FromHttpApiData
    , ToField
    , FromField
    )


instance OpenApi.ToParamSchema UsCityId where
  toParamSchema _ = do
    OpenApi.toSchema @Integer Proxy
      & OpenApi.title ?~ "UsCityId"
      & OpenApi.description ?~ "Unique identifier for a US city."
      & OpenApi.example ?~ Aeson.toJSON @UsCityId 123


instance OpenApi.ToSchema UsCityId where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsCityId") $
      OpenApi.toParamSchema @UsCityId Proxy
