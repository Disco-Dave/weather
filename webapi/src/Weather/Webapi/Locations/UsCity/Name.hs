module Weather.Webapi.Locations.UsCity.Name
  ( UsCityName
  , Error (..)
  , errorText
  , fromText
  , toText
  )
where

import Control.Lens ((?~))
import Data.Aeson qualified as Aeson
import Data.Coerce (coerce)
import Data.Function ((&))
import Data.OpenApi qualified as OpenApi
import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import Data.Text qualified as Text
import Database.PostgreSQL.Simple.ToField (ToField)


newtype UsCityName = UsCityName Text
  deriving (Show, Eq, Ord, Aeson.ToJSON, ToField)


data Error
  = Empty
  | LongerThan50Characters
  deriving (Show, Eq, Ord, Enum, Bounded)


errorText :: Error -> Text
errorText = \case
  Empty -> "US city name is empty."
  LongerThan50Characters -> "US city name is longer than 50 characters."


instance Aeson.ToJSON Error where
  toJSON = Aeson.toJSON . errorText
  toEncoding = Aeson.toEncoding . errorText


instance OpenApi.ToSchema Error where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsCityNameError") $
      OpenApi.toSchema @Text Proxy
        & OpenApi.title ?~ "UsCityNameError"
        & OpenApi.description ?~ "Error describing why a city name was rejected."
        & OpenApi.example ?~ Aeson.toJSON (minBound @Error)
        & OpenApi.enum_ ?~ fmap Aeson.toJSON [minBound @Error ..]


fromText :: Text -> Either Error UsCityName
fromText rawText
  | Text.null normalizedText = Left Empty
  | Text.length normalizedText > 50 = Left LongerThan50Characters
  | otherwise = Right $ UsCityName normalizedText
 where
  normalizedText = Text.strip rawText


toText :: UsCityName -> Text
toText =
  coerce


instance OpenApi.ToParamSchema UsCityName where
  toParamSchema _ = do
    OpenApi.toSchema @Text Proxy
      & OpenApi.title ?~ "UsCityName"
      & OpenApi.description ?~ "Name of a US city."
      & OpenApi.example ?~ Aeson.toJSON @Text "Chicago"


instance OpenApi.ToSchema UsCityName where
  declareNamedSchema _ = do
    pure . OpenApi.NamedSchema (Just "UsCityName") $
      OpenApi.toParamSchema @UsCityName Proxy
