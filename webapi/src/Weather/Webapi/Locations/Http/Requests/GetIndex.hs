module Weather.Webapi.Locations.Http.Requests.GetIndex
  ( GetIndexRequest (..)
  , GetIndexBadRequest (..)
  , toFindCitiesQueryParams
  )
where

import Data.Aeson qualified as Aeson
import Data.Bifunctor (Bifunctor (first))
import Data.Either (fromLeft)
import Data.Foldable (Foldable (toList))
import Data.OpenApi qualified as OpenApi
import Data.Text (Text)
import Env.Generic (Generic)
import Weather.Webapi.Locations.Queries.FindCities qualified as FindCities
import Weather.Webapi.Locations.UsCity.Name qualified as UsCityName
import Weather.Webapi.Locations.UsState.Abbreviation qualified as UsStateAbbreviation


data GetIndexRequest = GetIndexRequest
  { city :: Maybe Text
  , state :: Maybe Text
  }


data GetIndexBadRequest = GetIndexBadRequest
  { city :: [UsCityName.Error]
  , state :: [UsStateAbbreviation.Error]
  }
  deriving (Show, Eq, Generic)


instance Aeson.ToJSON GetIndexBadRequest
instance OpenApi.ToSchema GetIndexBadRequest


toFindCitiesQueryParams
  :: GetIndexRequest
  -> Either GetIndexBadRequest FindCities.QueryParams
toFindCitiesQueryParams request =
  let cityResult =
        first pure $ traverse UsCityName.fromText request.city
      stateResult =
        first toList $ traverse UsStateAbbreviation.fromText request.state
   in case (cityResult, stateResult) of
        (Right city, Right state) ->
          Right $ FindCities.QueryParams city state
        _ ->
          Left $
            GetIndexBadRequest
              { city = fromLeft [] cityResult
              , state = fromLeft [] stateResult
              }
