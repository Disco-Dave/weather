module Weather.Webapi
  ( runAppWith
  , runApp
  , start
  )
where

import GHC.Stack (HasCallStack)
import Weather.Shared.Exception (finalExceptionHandler)
import Weather.Shared.Logging (withExceptionLogging)
import Weather.Webapi.AppData (withAppData)
import Weather.Webapi.AppM (AppM (..))
import Weather.Webapi.AppM qualified as AppM
import Weather.Webapi.Config (Config (..))
import Weather.Webapi.Config qualified as Config
import Weather.Webapi.Http qualified as Http


runAppWith :: HasCallStack => Config -> AppM a -> IO a
runAppWith config app =
  finalExceptionHandler . withAppData config $ \appData ->
    AppM.runAppM appData (withExceptionLogging maxBound app)


runApp :: HasCallStack => AppM a -> IO a
runApp app = do
  config <- Config.fromEnvironmentVariables
  runAppWith config app


start :: HasCallStack => Config -> IO ()
start config =
  runAppWith config $
    Http.start config.http
