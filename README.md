# Weather
Fullstack application for retrieving (mock) weather forecasts for US Cities.
Currently the database is seeded with the top 10 most populated cities in the US.

Here is the complete list of cities:
  - New York, NY
  - Los Angeles, CA
  - Chicago, IL
  - Houston, TX
  - Phoenix, AZ
  - Philadelphia, PA
  - San Antonio, TX
  - San Diego, CA
  - Dallas, TX
  - San Jose, CA


## Projects
- [images](./images) - Custom docker images for packaging our software.
- [shared](./shared) - Shared library for the Haskell projects.
- [database](./database) - Manages schema changes for our PostgreSQL database.
- [webapi](./webapi) - REST API for looking up locations and weather.
- [frontend](./frontend) - Elm fronted for looking up locations and weather


## Getting Started

### Environment Variables
Install/setup [direnv](https://direnv.net/) and then run the following commands:
```
echo 'source .envrc.sample' > .envrc
direnv allow
```

**IMPORTANT** You may need to adjust `WEATHER_WEBAPI_DATABASE_MAX_CONNECTIONS` to be something greater than or equal to your cpu core count. 
You can achieve this by modifying your `.envrc` file like so:
```
source .envrc.sample

export WEATHER_WEBAPI_DATABASE_MAX_CONNECTIONS=16
```
And then run `direnv allow` again.


### Using Docker
If you want to run the entire application in docker then execute the following:
```
docker compose up -d
```


### Native
[shared](./shared), [database](./database), and [webapi](./webapi) are all Haskell projects that can be built with [stack](https://docs.haskellstack.org/en/stable/).

You can build all the Haskell projects at once from the root directory by running:
```
stack build
```

You may also test all the Haskell projects at once from the root directory by running:
```
stack test
```

To run the database migrations:
```
stack run weather-database
```

To start the REST API:
```
stack run weather-webapi
```

The [frontend](./frontend) is a node project built with Elm, to get started with it run the following:
```
cd frontend
npm clean-install
```

You can then start the development server with HMR:
```
npm run serve
```

Or you may run the test suite:
```
npm run test
```

Or you may build the production bundle:
```
npm run build
```

### Links

Once everything is up, then the following will become available:
- http://localhost/3000/docs - OpenAPI documentation for the REST API
- http://localhost:4000 - Elm frontend

Note, you can change these ports by modifying `WEATHER_WEBAPI_HTTP_PORT` and `WEATHER_FRONTEND_PORT`.
