module Weather.Shared.DatabaseSpec
  ( spec
  )
where

import Control.Monad.Reader (ReaderT (runReaderT))
import Database.PostgreSQL.Simple qualified as Postgres
import Test.Hspec (Spec, aroundAll, describe, it, shouldBe)
import Weather.Shared.Database (DatabaseConfig (..))
import Weather.Shared.Database qualified as Database
import Weather.Shared.Database.ConnectionUrlSpec (withConnectionUrl)
import Weather.Shared.Database.IdleConnectionTimeout qualified as IdleConnectionTimeout
import Weather.Shared.Database.MaxConnections qualified as MaxConnections


spec :: Spec
spec =
  aroundAll withConnectionUrl $ do
    describe "withConnectionPool" $
      it "can create a connection pool" $ \connectionUrl -> do
        let config =
              DatabaseConfig
                { connectionUrl
                , idleConnectionTimeout = IdleConnectionTimeout.def
                , maxConnections = MaxConnections.def
                }

        Database.withConnectionPool config $ \_pool ->
          True `shouldBe` True

    describe "withConnection" $
      it "can use a connection from the pool" $ \connectionUrl -> do
        let config =
              DatabaseConfig
                { connectionUrl
                , idleConnectionTimeout = IdleConnectionTimeout.def
                , maxConnections = MaxConnections.def
                }

        Database.withConnectionPool config $ \pool -> do
          [Postgres.Only number] <- flip runReaderT pool . Database.withConnection $ \connection ->
            Postgres.query_ @(Postgres.Only Integer) connection "select 3;"

          number `shouldBe` 3
