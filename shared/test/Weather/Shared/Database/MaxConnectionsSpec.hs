module Weather.Shared.Database.MaxConnectionsSpec
  ( TestMaxConnections (..)
  , spec
  )
where

import Data.Maybe (mapMaybe)
import GHC.Conc (numCapabilities)
import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck qualified as QuickCheck
import Weather.Shared.Database.MaxConnections (MaxConnections)
import Weather.Shared.Database.MaxConnections qualified as MaxConnections


newtype TestMaxConnections = TestMaxConnections MaxConnections
  deriving (Show, Eq)


instance QuickCheck.Arbitrary TestMaxConnections where
  arbitrary =
    let generateInt =
          QuickCheck.chooseInt (numCapabilities, 999_999_999)
        toTestValue =
          fmap TestMaxConnections . MaxConnections.fromInt
     in QuickCheck.suchThatMap generateInt toTestValue


  shrink (TestMaxConnections connections) =
    let smallerInts =
          QuickCheck.shrink . QuickCheck.Positive $ MaxConnections.toInt connections

        toTestValue =
          fmap TestMaxConnections . MaxConnections.fromInt . QuickCheck.getPositive
     in mapMaybe toTestValue smallerInts


fromInt :: Int -> Maybe Int
fromInt =
  fmap MaxConnections.toInt . MaxConnections.fromInt


spec :: Spec
spec = do
  prop "allows all numbers greater than numCapabilities" $ \(TestMaxConnections _) ->
    True `shouldBe` True

  prop "rejects all numbers less than or equal to 0" $ \(QuickCheck.NonPositive connections) -> do
    fromInt connections `shouldBe` Nothing
