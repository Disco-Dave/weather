module Weather.Shared.Database
  ( DatabaseConfig (..)
  , withConnectionPool
  , withConnection
  , NoSuchColumn (..)
  , fieldByNameWith
  , fieldByName
  )
where

import Control.Exception (Exception)
import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Reader (MonadReader, ask, asks)
import Control.Monad.State.Lazy (put)
import Control.Monad.Trans (lift)
import Data.ByteString (ByteString)
import Data.Has (Has (..))
import Data.Pool (Pool)
import Data.Pool qualified as Pool
import Database.PostgreSQL.LibPQ qualified as LibPQ
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.FromField (FieldParser, FromField (fromField))
import Database.PostgreSQL.Simple.FromRow (RowParser)
import Database.PostgreSQL.Simple.Internal qualified as PostgresInternal
import GHC.Stack (HasCallStack)
import UnliftIO (MonadUnliftIO)
import UnliftIO.Exception (bracket)
import Weather.Shared.Database.ConnectionUrl (ConnectionUrl)
import Weather.Shared.Database.ConnectionUrl qualified as ConnectionUrl
import Weather.Shared.Database.IdleConnectionTimeout (IdleConnectionTimeout)
import Weather.Shared.Database.IdleConnectionTimeout qualified as IdleConnectionTimeout
import Weather.Shared.Database.MaxConnections (MaxConnections)
import Weather.Shared.Database.MaxConnections qualified as MaxConnections


data DatabaseConfig = DatabaseConfig
  { connectionUrl :: ConnectionUrl
  , idleConnectionTimeout :: IdleConnectionTimeout
  , maxConnections :: MaxConnections
  }


withConnectionPool :: (MonadUnliftIO m, HasCallStack) => DatabaseConfig -> (Pool Postgres.Connection -> m a) -> m a
withConnectionPool config use =
  checkpointCallStack $
    let createPool =
          liftIO . Pool.newPool $
            Pool.defaultPoolConfig
              (ConnectionUrl.open config.connectionUrl)
              ConnectionUrl.close
              (IdleConnectionTimeout.toDouble config.idleConnectionTimeout)
              (MaxConnections.toInt config.maxConnections)
        destroyPool = liftIO . Pool.destroyAllResources
     in bracket createPool destroyPool use


withConnection
  :: ( MonadUnliftIO m
     , MonadReader r m
     , Has (Pool Postgres.Connection) r
     , HasCallStack
     )
  => (Postgres.Connection -> IO a)
  -> m a
withConnection useConnection =
  checkpointCallStack $ do
    connectionPool <- asks getter
    liftIO $ Pool.withResource connectionPool useConnection


-- | Thrown when there is no column of the given name.
newtype NoSuchColumn = NoSuchColumn ByteString
  deriving (Show, Eq, Ord)


instance Exception NoSuchColumn


-- | This is similar to 'fieldWith' but instead of trying to
-- deserialize the field at the current position it goes through all
-- fields in the current row (starting at the beginning not the
-- current position) and tries to deserialize the first field with a
-- matching column name.
fieldByNameWith :: FieldParser a -> ByteString -> RowParser a
fieldByNameWith fieldParser columnName =
  PostgresInternal.RP $ do
    PostgresInternal.Row{rowresult, row} <- ask

    numberOfColumns <-
      lift . lift . PostgresInternal.liftConversion $
        LibPQ.nfields rowresult

    lift $ put numberOfColumns

    maybeTargetColumn <-
      lift . lift . PostgresInternal.liftConversion $ do
        let columns = [LibPQ.Col 0 .. numberOfColumns - 1]

            search column prev = do
              name <- LibPQ.fname rowresult column

              if name == Just columnName
                then pure (Just column)
                else prev

        foldr search (pure Nothing) columns

    case maybeTargetColumn of
      Nothing ->
        lift . lift . PostgresInternal.conversionError $
          NoSuchColumn columnName
      Just targetColumn ->
        lift . lift $ do
          oid <- PostgresInternal.liftConversion (LibPQ.ftype rowresult targetColumn)
          val <- PostgresInternal.liftConversion (LibPQ.getvalue rowresult row targetColumn)
          fieldParser (PostgresInternal.Field rowresult targetColumn oid) val


-- | This is a wrapper around 'fieldByNameWith' that gets the
-- 'FieldParser' via the typeclass instance. Take a look at the docs
-- for 'fieldByNameWith' for the details of this function.
fieldByName :: FromField a => ByteString -> RowParser a
fieldByName =
  fieldByNameWith fromField
