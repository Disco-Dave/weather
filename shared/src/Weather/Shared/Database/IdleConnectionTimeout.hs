module Weather.Shared.Database.IdleConnectionTimeout
  ( IdleConnectionTimeout
  , fromDouble
  , toDouble
  , def
  )
where

import Data.Coerce (coerce)


-- | How long an idle connection stay open for in __SECONDS__.
newtype IdleConnectionTimeout = IdleConnectionTimeout Double
  deriving (Show, Eq)


-- | Smallest unit is 0.5 seconds, so anything less than 0.5 will be nothing.
fromDouble :: Double -> Maybe IdleConnectionTimeout
fromDouble double
  | double >= 0.5 = Just $ IdleConnectionTimeout double
  | otherwise = Nothing


-- | Convert the 'IdleConnectionTimeout' to a 'Double' in __SECONDS__.
toDouble :: IdleConnectionTimeout -> Double
toDouble =
  coerce


-- | Default value of an 'IdleConnectionTimeout', 15 seconds.
def :: IdleConnectionTimeout
def =
  IdleConnectionTimeout 15
