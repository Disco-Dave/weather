module Weather.Shared.Database.MaxConnections
  ( MaxConnections
  , fromInt
  , toInt
  , def
  )
where

import Data.Coerce (coerce)
import GHC.Conc (numCapabilities)


-- | The max amount of connections to have open at once.
newtype MaxConnections = MaxConnections Int
  deriving (Show, Eq)


-- | Converts an 'Int' to 'MaxConnections' if it's greater than or equal to 'numCapabilities'.
fromInt :: Int -> Maybe MaxConnections
fromInt int
  | int >= numCapabilities = Just $ MaxConnections int
  | otherwise = Nothing


-- | Downgrade the 'ConnectionUrl' to an unvalidated 'ByteString'.
toInt :: MaxConnections -> Int
toInt =
  coerce


-- | Defaults to 'numCapabilities'.
def :: MaxConnections
def =
  MaxConnections numCapabilities
