CREATE TABLE public.us_cities (
  id BIGSERIAL NOT NULL PRIMARY KEY,

  name TEXT NOT NULL,
  state CHAR(2) NOT NULL REFERENCES public.us_states(abbreviation),

  created_at TIMESTAMPTZ NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT current_timestamp
);

CREATE UNIQUE INDEX public_us_cities_name_state_idx ON public.us_cities(name, state);

-- Just the top 10 cities sorted by population based of the "2021 estimate"
-- https://en.wikipedia.org/wiki/List_of_United_States_cities_by_population
INSERT INTO public.us_cities (name, state)
VALUES
  ('New York', 'NY'),
  ('Los Angeles', 'CA'),
  ('Chicago', 'IL'),
  ('Houston', 'TX'),
  ('Phoenix', 'AZ'),
  ('Philadelphia', 'PA'),
  ('San Antonio', 'TX'),
  ('San Diego', 'CA'),
  ('Dallas', 'TX'),
  ('San Jose', 'CA');
