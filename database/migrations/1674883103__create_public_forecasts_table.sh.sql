CREATE TABLE public.forecasts (
  id BIGSERIAL NOT NULL PRIMARY KEY,

  city_id BIGINT NOT NULL REFERENCES public.us_cities(id) UNIQUE,

  temperature FLOAT8 NOT NULL,
  condition TEXT NOT NULL,

  created_at TIMESTAMPTZ NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT current_timestamp
);

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 31.23, 'Cloudy'
FROM public.us_cities AS city
WHERE city.name = 'New York' AND city.state = 'NY';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 77.8, 'Sunny'
FROM public.us_cities AS city
WHERE city.name = 'Los Angeles' AND city.state = 'CA';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 43.42, 'Overcast'
FROM public.us_cities AS city
WHERE city.name = 'Chicago' AND city.state = 'IL';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 82.16, 'Sunny'
FROM public.us_cities AS city
WHERE city.name = 'Houston' AND city.state = 'TX';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 68.34, 'Sunny'
FROM public.us_cities AS city
WHERE city.name = 'Phoenix' AND city.state = 'AZ';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 28.09, 'Snow'
FROM public.us_cities AS city
WHERE city.name = 'Philadelphia' AND city.state = 'PA';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 31.23, 'Windy'
FROM public.us_cities AS city
WHERE city.name = 'San Antonio' AND city.state = 'TX';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 31.23, 'Cloudy'
FROM public.us_cities AS city
WHERE city.name = 'San Diego' AND city.state = 'CA';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 78.90, 'Tornado'
FROM public.us_cities AS city
WHERE city.name = 'Dallas' AND city.state = 'TX';

INSERT INTO public.forecasts (city_id, temperature, condition)
SELECT city.id, 72, 'Cloudy'
FROM public.us_cities AS city
WHERE city.name = 'San Jose' AND city.state = 'CA';
