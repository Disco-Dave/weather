module Main (main) where

import Weather.Database qualified as Database
import Weather.Database.Config qualified as Config


main :: IO ()
main = do
  config <- Config.fromEnvironmentVariables
  Database.start config
