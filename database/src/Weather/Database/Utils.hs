module Weather.Database.Utils
  ( symbolText
  )
where

import Data.Proxy (Proxy (Proxy))
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.TypeLits (KnownSymbol, symbolVal)


symbolText :: forall symbol. KnownSymbol symbol => Text
symbolText =
  Text.pack $ symbolVal @symbol Proxy
