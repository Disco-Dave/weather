module Weather.Database.DatabaseName
  ( DatabaseName (..)
  , getDatabaseName
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Reader (asks)
import Data.String (IsString)
import Data.Text (Text)
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.SqlQQ (sql)
import GHC.Stack (HasCallStack)
import Katip qualified
import Weather.Database.AppData (AppData (..))
import Weather.Database.AppM (AppM)


newtype DatabaseName = DatabaseName
  { unwrap :: Text
  }
  deriving (Show, Eq, Ord, IsString)


getDatabaseName :: HasCallStack => AppM DatabaseName
getDatabaseName =
  checkpointCallStack $ do
    Katip.logLocM Katip.DebugS "Querying for the database name"

    databaseName <- do
      connection <- asks (.connection)

      [Postgres.Only rawDbName] <-
        let query = [sql| SELECT current_database() |]
         in liftIO $ Postgres.query_ connection query

      pure $ DatabaseName rawDbName

    Katip.katipAddContext (Katip.sl "databaseName" databaseName.unwrap) $ do
      Katip.logLocM Katip.DebugS "Query for database name succeeded"
      pure databaseName
