module Weather.Database.Roles
  ( SchemaRoles (..)
  , Schemas (..)
  , noSchemas
  , TableRoles (..)
  , TableRoles_
  , Tables (..)
  , noTables
  , Roles (..)
  , noRoles
  , apply
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import GHC.Stack (HasCallStack)
import Weather.Database.AppM (AppM)
import Weather.Database.Roles.Schema (SchemaRoles (..), schema)
import Weather.Database.Roles.Table (TableRoles (..), TableRoles_, table)
import Weather.Database.Roles.User (user)
import Weather.Database.User (User (..))


newtype Schemas = Schemas
  { public :: SchemaRoles "public"
  }
  deriving (Show)


noSchemas :: Schemas
noSchemas =
  let noAccess = SchemaRoles False
   in Schemas
        { public = noAccess
        }


data Tables = Tables
  { publicUsStates :: TableRoles_ "public.us_states"
  , publicUsCities :: TableRoles "public.us_cities" "public.us_cities_id_seq"
  , publicForecasts :: TableRoles "public.forecasts" "public.forecasts_id_seq"
  }
  deriving (Show)


noTables :: Tables
noTables =
  let noAccess = TableRoles False False False False
   in Tables
        { publicUsStates = noAccess
        , publicUsCities = noAccess
        , publicForecasts = noAccess
        }


data Roles = Roles
  { schemas :: Schemas
  , tables :: Tables
  }
  deriving (Show)


noRoles :: Roles
noRoles =
  Roles
    { schemas = noSchemas
    , tables = noTables
    }


apply :: HasCallStack => User -> Roles -> AppM ()
apply creds roles =
  checkpointCallStack $ do
    user creds

    schema creds.username roles.schemas.public

    table creds.username roles.tables.publicUsStates
    table creds.username roles.tables.publicUsCities
    table creds.username roles.tables.publicForecasts
