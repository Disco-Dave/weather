module Weather.Database.MigrationDir
  ( MigrationDir
  , defaultDir
  , fromFilePath
  , toFilePath
  )
where

import Data.Coerce (coerce)
import Data.Text qualified as Text


newtype MigrationDir = MigrationDir FilePath


defaultDir :: MigrationDir
defaultDir =
  MigrationDir "migrations"


fromFilePath :: FilePath -> Maybe MigrationDir
fromFilePath filePath =
  let strippedFilePath = Text.strip $ Text.pack filePath
   in if Text.null strippedFilePath
        then Nothing
        else Just . MigrationDir $ Text.unpack strippedFilePath


toFilePath :: MigrationDir -> FilePath
toFilePath =
  coerce
