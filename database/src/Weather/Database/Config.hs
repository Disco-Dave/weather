-- | Module for parsing configuration out of environment variables
module Weather.Database.Config
  ( Config (..)
  , parser
  , fromEnvironmentVariables
  )
where

import Data.Bifunctor (Bifunctor (first))
import Data.Char qualified as Char
import Data.Text qualified as Text
import Env qualified
import Weather.Database.MigrationDir (MigrationDir)
import Weather.Database.MigrationDir qualified as MigrationDir
import Weather.Database.Password (Password)
import Weather.Database.Password qualified as Password
import Weather.Database.User (User (..), Users (..))
import Weather.Database.Username (Username)
import Weather.Database.Username qualified as Username
import Weather.Shared.Database.ConnectionUrl (ConnectionUrl)
import Weather.Shared.EnvParsers (connectionUrl, loggingConfig, parseString)
import Weather.Shared.Logging (LoggingConfig)


migrationDir :: Env.Reader Env.Error MigrationDir
migrationDir =
  parseString $ \rawString ->
    case MigrationDir.fromFilePath rawString of
      Nothing -> Left "Migration directory may not be set to empty path."
      Just value -> pure value


username :: Env.Reader Env.Error Username
username =
  parseString (first (Text.pack . show) . Username.fromText)


password :: Env.Reader Env.Error Password
password =
  parseString (first (Text.pack . show) . Password.fromText)


user :: String -> Env.Parser Env.Error User
user name =
  User
    <$> Env.var username (fmap Char.toUpper name <> "_USERNAME") (Env.help $ "Username for the " <> name <> " postgres user.")
    <*> Env.sensitive (Env.var password (fmap Char.toUpper name <> "_PASSWORD") (Env.help $ "Password for the " <> name <> " postgres user."))


users :: Env.Parser Env.Error Users
users =
  Users
    <$> user "webapi"


data Config = Config
  { logging :: LoggingConfig
  , connectionUrl :: ConnectionUrl
  , users :: Users
  , migrationDir :: MigrationDir
  }


parser :: Env.Parser Env.Error Config
parser =
  Env.prefixed "WEATHER_DATABASE_" $
    Config
      <$> loggingConfig
      <*> Env.var connectionUrl "URL" (Env.help "Connection URL used to migrate the database.")
      <*> users
      <*> Env.var migrationDir "MIGRATION_DIR" (Env.help "Directory where the migration scripts are located" <> Env.def MigrationDir.defaultDir)


fromEnvironmentVariables :: IO Config
fromEnvironmentVariables =
  Env.parse (Env.header "weather-database") parser
