-- | Type and functions for parsing a PostgreSQL username.
module Weather.Database.Username
  ( Username
  , toText
  , fromText
  )
where

import Control.Monad (when)
import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Text.Parsec qualified as Parsec


-- | Name of PostgreSQL role with LOGIN, essentially a "user".
newtype Username = Username Text
  deriving (Show, Eq)


-- | Downgrade from 'Username' to an unvalidated 'Text'.
toText :: Username -> Text
toText =
  coerce


-- | Parse an unvalidated 'Text' into a 'Username' if:
-- 1. Starts with a character in a range of a to z, A to Z, or underscore.
-- 2. Subsequent characters are in a range of a to z, A to Z, 0 to 9, underscore, or money sign.
-- 3. Between 1 and 63 characters of length.
--
-- /Note/ Leading and trailing white space will be stripped off before parsing.
fromText :: Text -> Either Parsec.ParseError Username
fromText text =
  let parser = do
        let alwaysOkChars =
              mconcat
                [ ['a' .. 'z']
                , ['A' .. 'Z']
                , ['_']
                ]

        firstChar <-
          Parsec.oneOf alwaysOkChars

        subsequentChars <-
          Parsec.many . Parsec.oneOf . mconcat $
            [ alwaysOkChars
            , ['0' .. '9']
            , ['$']
            ]

        let parsedValue =
              Text.pack $
                firstChar : subsequentChars

        when (Text.length parsedValue > 63) $
          Parsec.unexpected "Length longer than 63 characters"

        pure $ Username parsedValue
   in Parsec.parse parser "" (Text.strip text)
