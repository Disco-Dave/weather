module Weather.Database.Migrate (migrate) where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Reader (asks)
import Data.Text
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.Migration qualified as PgMigration
import Database.PostgreSQL.Simple.SqlQQ (sql)
import GHC.Stack (HasCallStack)
import Katip qualified
import UnliftIO (MonadUnliftIO (withRunInIO), askRunInIO)
import UnliftIO.Exception (throwString)
import Weather.Database.AppData (AppData (..))
import Weather.Database.AppM (AppM)
import Weather.Database.MigrationDir (MigrationDir)
import Weather.Database.MigrationDir qualified as MigrationDir
import Weather.Database.Roles qualified as Roles
import Weather.Database.User (Users (..))
import Weather.Database.User.Webapi qualified as Webapi


applyScripts :: HasCallStack => MigrationDir -> AppM ()
applyScripts migrationDir =
  checkpointCallStack $ do
    connection <- asks (.connection)

    let scriptsFolder = MigrationDir.toFilePath migrationDir

    Katip.katipAddContext (Katip.sl "scriptsFolder" scriptsFolder) $
      Katip.logLocM Katip.InfoS "Attempting to run migrations scripts"

    runInIO <- askRunInIO

    let logWriter =
          \case
            Left err ->
              runInIO $ Katip.logLocM Katip.ErrorS (Katip.logStr err)
            Right message ->
              runInIO $ Katip.logLocM Katip.DebugS (Katip.logStr message)

    let options =
          PgMigration.defaultOptions
            { PgMigration.optLogWriter = logWriter
            , PgMigration.optVerbose = PgMigration.Verbose
            , PgMigration.optTransactionControl = PgMigration.NoNewTransaction
            }

        command =
          mconcat
            [ PgMigration.MigrationInitialization
            , PgMigration.MigrationDirectory scriptsFolder
            ]

    result <-
      liftIO $ PgMigration.runMigration connection options command

    case result of
      PgMigration.MigrationSuccess ->
        Katip.logLocM Katip.InfoS "Finished running migration scripts"
      PgMigration.MigrationError err ->
        throwString err


applyUsers :: HasCallStack => Users -> AppM ()
applyUsers users =
  checkpointCallStack $ do
    Roles.apply users.webapi Webapi.roles


migrate :: HasCallStack => MigrationDir -> Users -> AppM ()
migrate migrationDir users =
  checkpointCallStack $ do
    connection <- asks (.connection)

    withRunInIO $ \runInIO ->
      Postgres.withTransaction connection . runInIO $ do
        Katip.katipAddContext
          (Katip.sl "jobType" ("migrationScripts" :: Text))
          (applyScripts migrationDir)

        Katip.katipAddContext (Katip.sl "jobType" ("users" :: Text)) $ do
          let query = [sql| REVOKE ALL ON SCHEMA public FROM public; |]
           in liftIO . void $ Postgres.execute_ connection query

          applyUsers users
