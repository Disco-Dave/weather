-- | Types and functions for parsing a PostgreSQL login password.
module Weather.Database.Password
  ( Password
  , toText
  , fromText
  )
where

import Control.Monad (when)
import Data.Coerce (coerce)
import Data.Text (Text)
import Data.Text qualified as Text
import Text.Parsec qualified as Parsec


-- | Password given to a PostgreSQL user.
newtype Password = Password Text


-- | Downgrade from 'Password' to an unvalidated 'Text'.
toText :: Password -> Text
toText =
  coerce


-- | Parse an unvalidated 'Text' into a 'Password' if:
-- 1. Between 1 and 300 characters of length.
fromText :: Text -> Either Parsec.ParseError Password
fromText =
  let parser = do
        firstChar <- Parsec.anyChar
        subsequentChars <- Parsec.many Parsec.anyChar

        let parsedValue =
              Text.pack $
                firstChar : subsequentChars

        when (Text.length parsedValue > 300) $
          Parsec.unexpected "Length longer than 300 characters"

        pure $ Password parsedValue
   in Parsec.parse parser ""
