module Weather.Database.AppData
  ( AppData (..)
  , withAppData
  )
where

import Control.Monad.Cont (ContT (..))
import Data.Has (Has (..))
import Database.PostgreSQL.Simple qualified as Postgres
import Katip.Monadic (KatipContextTState)
import Weather.Database.Config (Config)
import Weather.Database.Config qualified as Config
import Weather.Shared.Database.ConnectionUrl qualified as ConnectionUrl
import Weather.Shared.Logging (withLoggingData)
import Prelude hiding (log)


data AppData = AppData
  { logging :: KatipContextTState
  , connection :: Postgres.Connection
  }


instance Has KatipContextTState AppData where
  getter = (.logging)
  modifier f original =
    let updated = f original.logging
     in original{logging = updated}


instance Has Postgres.Connection AppData where
  getter = (.connection)
  modifier f original =
    let updated = f original.connection
     in original{connection = updated}


withAppData :: Config -> (AppData -> IO a) -> IO a
withAppData config =
  runContT $ do
    logging <- ContT $ withLoggingData "weather-database" () config.logging
    connection <- ContT $ ConnectionUrl.withConnection config.connectionUrl

    pure $ AppData{..}
