module Weather.Database.User
  ( User (..)
  , Users (..)
  )
where

import Weather.Database.Password (Password)
import Weather.Database.Username (Username)


-- | Username and password for a PostgreSQL user.
data User = User
  { username :: Username
  , password :: Password
  }


-- | Record of all users this program manages.
newtype Users = Users
  { webapi :: User
  }
