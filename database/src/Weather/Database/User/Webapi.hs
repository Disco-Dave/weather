module Weather.Database.User.Webapi
  ( roles
  )
where

import Weather.Database.Roles (Roles (..), SchemaRoles (..), TableRoles (..))
import Weather.Database.Roles qualified as Roles


roles :: Roles
roles =
  Roles.noRoles
    { Roles.schemas =
        Roles.noSchemas
          { Roles.public = SchemaRoles{usage = True}
          }
    , Roles.tables =
        Roles.noTables
          { Roles.publicUsStates =
              TableRoles
                { insert = False
                , select = True
                , update = False
                , delete = False
                }
          , Roles.publicUsCities =
              TableRoles
                { insert = False
                , select = True
                , update = False
                , delete = False
                }
          , Roles.publicForecasts =
              TableRoles
                { insert = False
                , select = True
                , update = False
                , delete = False
                }
          }
    }
