module Weather.Database.AppM
  ( AppM (..)
  , runAppM
  )
where

import Control.Exception.Annotated.UnliftIO (checkpoint)
import Control.Monad.Catch (MonadCatch, MonadThrow)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader (MonadReader, ReaderT (runReaderT))
import GHC.Stack (HasCallStack)
import Katip qualified
import UnliftIO (MonadUnliftIO)
import Weather.Database.AppData (AppData (..))
import Weather.Shared.Exception.Annotations qualified as Annotations
import Weather.Shared.Logging (ReaderWithLoggingConfig (..))


newtype AppM a = AppM (ReaderT AppData IO a)
  deriving
    ( Functor
    , Applicative
    , Monad
    , MonadFail
    , MonadIO
    , MonadUnliftIO
    , MonadThrow
    , MonadCatch
    , MonadReader AppData
    )
  deriving (Katip.Katip, Katip.KatipContext) via ReaderWithLoggingConfig AppM AppData


runAppM :: HasCallStack => AppData -> AppM value -> IO value
runAppM appData app =
  let (AppM modifiedApp) = checkpoint Annotations.appMonadWasSetup app
   in runReaderT modifiedApp appData
