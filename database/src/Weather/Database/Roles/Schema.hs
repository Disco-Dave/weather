module Weather.Database.Roles.Schema
  ( SchemaRoles (..)
  , schema
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (asks)
import Data.Aeson qualified as Aeson
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Database.PostgreSQL.Simple.Types qualified as PgTypes
import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import GHC.TypeLits (KnownSymbol, Symbol)
import Katip qualified
import Weather.Database.AppData (AppData (..))
import Weather.Database.AppM (AppM)
import Weather.Database.Username (Username)
import Weather.Database.Username qualified as Username
import Weather.Database.Utils (symbolText)


newtype SchemaRoles (schemaName :: Symbol) = SchemaRoles
  { usage :: Bool
  }
  deriving (Show, Eq, Generic)
instance Aeson.ToJSON (SchemaRoles schemaName)


schema :: forall schemaName. (HasCallStack, KnownSymbol schemaName) => Username -> SchemaRoles schemaName -> AppM ()
schema username roles = do
  let schemaName = symbolText @schemaName

      logContext =
        mconcat
          [ Katip.sl "roles" roles
          , Katip.sl "schema" schemaName
          , Katip.sl "username" (Username.toText username)
          ]

  checkpointCallStack . Katip.katipAddContext logContext $ do
    let message
          | roles.usage = "Granting usage for schema"
          | otherwise = "Revoking all access for schema"
     in Katip.logLocM Katip.DebugS message

    connection <- asks (.connection)

    let query
          | roles.usage = [sql| GRANT USAGE ON SCHEMA ? TO ?; |]
          | otherwise = [sql| REVOKE ALL ON SCHEMA ? FROM ?; |]
        params =
          ( PgTypes.Identifier schemaName
          , PgTypes.Identifier $ Username.toText username
          )
     in void . liftIO $ Postgres.execute connection query params

    let message
          | roles.usage = "Granted usage for schema"
          | otherwise = "Revoked all access for schema"
     in Katip.logLocM Katip.InfoS message
