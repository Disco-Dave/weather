module Weather.Database.Roles.Table
  ( TableRoles (..)
  , TableRoles_
  , table
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (asks)
import Data.Aeson qualified as Aeson
import Data.Text qualified as Text
import Data.Text.Encoding
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Database.PostgreSQL.Simple.Types qualified as PgTypes
import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import GHC.TypeLits (KnownSymbol, Symbol)
import Katip qualified
import Weather.Database.AppData (AppData (..))
import Weather.Database.AppM (AppM)
import Weather.Database.Username (Username)
import Weather.Database.Username qualified as Username
import Weather.Database.Utils (symbolText)


data TableRoles (tableName :: Symbol) (seqName :: Symbol) = TableRoles
  { insert :: Bool
  , select :: Bool
  , update :: Bool
  , delete :: Bool
  }
  deriving (Show, Eq, Generic)
instance Aeson.ToJSON (TableRoles tableName seqName)


type TableRoles_ tableName = TableRoles tableName ""


table
  :: forall tableName seqName
   . (HasCallStack, KnownSymbol tableName, KnownSymbol seqName)
  => Username
  -> TableRoles tableName seqName
  -> AppM ()
table username roles = do
  let tableName = symbolText @tableName

      maybeSeqName =
        let seqName = Text.strip $ symbolText @seqName
         in if Text.null seqName
              then Nothing
              else Just seqName

      logContext =
        mconcat
          [ Katip.sl "roles" roles
          , Katip.sl "table" tableName
          , foldMap (Katip.sl "sequence") maybeSeqName
          , Katip.sl "username" (Username.toText username)
          ]

      parseRelation relation =
        let pieces =
              [ piece
              | rawPiece <- Text.split (== '.') relation
              , let piece = Text.strip rawPiece
              , not $ Text.null piece
              ]
         in case pieces of
              (schemaPiece : relationPiece : _) ->
                PgTypes.QualifiedIdentifier (Just schemaPiece) relationPiece
              (relationPiece : _) ->
                PgTypes.QualifiedIdentifier Nothing relationPiece
              _ ->
                PgTypes.QualifiedIdentifier Nothing relation

      usernameParam =
        PgTypes.Identifier (Username.toText username)

      tableParam =
        parseRelation tableName

  checkpointCallStack . Katip.katipAddContext logContext $ do
    connection <- asks (.connection)

    let applyRole hasRole roleName = do
          let message
                | hasRole roles = "Granting " <> Katip.ls roleName <> " for table"
                | otherwise = "Revoking " <> Katip.ls roleName <> " for table"
           in Katip.logLocM Katip.DebugS message

          let query
                | hasRole roles =
                    PgTypes.Query $ "GRANT " <> encodeUtf8 roleName <> " ON TABLE ? TO ?;"
                | otherwise =
                    PgTypes.Query $ "REVOKE " <> encodeUtf8 roleName <> " ON TABLE ? FROM ?;"

              params =
                ( tableParam
                , usernameParam
                )
           in void . liftIO $ Postgres.execute connection query params

          let message
                | hasRole roles = "Granted " <> Katip.ls roleName <> " for table"
                | otherwise = "Revoked " <> Katip.ls roleName <> " for table"
           in Katip.logLocM Katip.InfoS message

    case maybeSeqName of
      Nothing -> pure ()
      Just seqName -> do
        let message
              | roles.insert = "Granting USAGE for sequence"
              | otherwise = "Revoking USAGE for sequence"
         in Katip.logLocM Katip.DebugS message

        let query
              | roles.insert =
                  [sql| GRANT USAGE ON SEQUENCE ? TO ?; |]
              | otherwise =
                  [sql| REVOKE USAGE ON SEQUENCE ? FROM ?; |]
            params =
              ( parseRelation seqName
              , usernameParam
              )
         in void . liftIO $ Postgres.execute connection query params

        let message
              | roles.insert = "Granted USAGE for sequence"
              | otherwise = "Revoked USAGE for sequence"
         in Katip.logLocM Katip.InfoS message

    applyRole (.insert) "INSERT"
    applyRole (.select) "SELECT"
    applyRole (.update) "UPDATE"
    applyRole (.delete) "DELETE"
