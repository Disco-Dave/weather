module Weather.Database.Roles.User
  ( user
  )
where

import Control.Exception.Annotated.UnliftIO (checkpointCallStack)
import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (asks)
import Database.PostgreSQL.Simple qualified as Postgres
import Database.PostgreSQL.Simple qualified as Simple
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Database.PostgreSQL.Simple.Types qualified as PgTypes
import GHC.Stack (HasCallStack)
import Katip qualified
import Weather.Database.AppData (AppData (..))
import Weather.Database.AppM (AppM)
import Weather.Database.DatabaseName (DatabaseName (..), getDatabaseName)
import Weather.Database.Password qualified as Password
import Weather.Database.User (User (..))
import Weather.Database.Username qualified as Username


user :: HasCallStack => User -> AppM ()
user creds = do
  let logContext =
        Katip.sl "user" (Username.toText creds.username)

  checkpointCallStack . Katip.katipAddContext logContext $ do
    connection <- asks (.connection)
    dbname <- getDatabaseName

    needsCreated <-
      let query =
            [sql|
              SELECT 1
              FROM pg_roles
              WHERE rolname = ?;
            |]
          params = Postgres.Only $ Username.toText creds.username
       in liftIO . fmap null $ Postgres.query @_ @(Simple.Only Integer) connection query params

    let message
          | needsCreated = "Creating role"
          | otherwise = "Updating password for role"
     in Katip.logLocM Katip.DebugS message

    let query =
          if needsCreated
            then
              [sql|
                CREATE ROLE ? WITH LOGIN PASSWORD ?;
              |]
            else
              [sql|
                ALTER ROLE ? WITH PASSWORD ?;
              |]
        params =
          ( PgTypes.Identifier $ Username.toText creds.username
          , Password.toText creds.password
          )
     in liftIO . void $ Postgres.execute connection query params

    let query =
          [sql|
            GRANT CONNECT ON DATABASE ? TO ?;
          |]
        params =
          ( PgTypes.Identifier dbname.unwrap
          , PgTypes.Identifier $ Username.toText creds.username
          )
     in liftIO . void $ Postgres.execute connection query params

    let message
          | needsCreated = "Role was successfully created"
          | otherwise = "Role was successfully updated"
     in Katip.logLocM Katip.InfoS message
