module Weather.Database
  ( runAppWith
  , runApp
  , start
  )
where

import GHC.Stack.Types (HasCallStack)
import Katip qualified
import Weather.Database.AppData (withAppData)
import Weather.Database.AppM (AppM, runAppM)
import Weather.Database.Config (Config (..))
import Weather.Database.Config qualified as Config
import Weather.Database.Migrate (migrate)
import Weather.Shared.Exception (finalExceptionHandler)
import Weather.Shared.Logging (withExceptionLogging)


runAppWith :: HasCallStack => Config -> AppM a -> IO a
runAppWith config app =
  finalExceptionHandler . withAppData config $ \appData ->
    runAppM appData (withExceptionLogging maxBound app)


runApp :: HasCallStack => AppM a -> IO a
runApp app = do
  config <- Config.fromEnvironmentVariables
  runAppWith config app


start :: HasCallStack => Config -> IO ()
start config = do
  runAppWith config $ do
    Katip.logLocM Katip.InfoS "Starting migration"

    migrate config.migrationDir config.users

    Katip.logLocM Katip.InfoS "Migration finished successfully"
