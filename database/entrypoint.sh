#!/bin/bash

set -e

echo "$WEATHER_DATABASE_URL"

until /usr/lib/postgresql/13/bin/pg_isready --dbname="$WEATHER_DATABASE_URL" &> /dev/null; do 
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

exec "$@"
