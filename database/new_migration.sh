#!/bin/bash

set -e

# Print the help text to stdout.
function print_help_text {
  cat << EOF
Create a new migration script for the database and
print its full file path to stdout.

Usage: $0 DESCRIPTION

Arguments:
  DESCRIPTION   Short description for the migration's file name   

Options:
  --help    Print this help message
EOF
}

# Short description is required in the first cli argument.
if [[ -z "$1" ]]; then
  print_help_text 2> /dev/stderr
  exit 1
fi

if [[ "$1" = "--help" ]]; then
  print_help_text
  exit
fi

# Get the real file path for this script, and then go up one level.
# Allows you to execute this script from any directory.
DATABASE_FOLDER="$(dirname "$(realpath "$0")")"
MIGRATION_FILE_PATH="$DATABASE_FOLDER/migrations/$(date +%s)__$1.sql"

# Create the new migration file and print its full path to stdout
touch "$MIGRATION_FILE_PATH"
echo "$MIGRATION_FILE_PATH"
